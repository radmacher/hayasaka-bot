ALTER TABLE users
  ADD time_online DATETIME;
ALTER TABLE users
  ADD last_seen DATETIME;

CREATE TABLE IF NOT EXISTS `tracker` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `guild_id` VARCHAR(64) NOT NULL,
  `date` DATETIME NOT NULL,
  `users_online` INT NULL,
  `users_idle` INT NULL,
  `users_dnd` INT NULL,
  `users_offline` INT NULL,
  `users_unknown` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARSET = utf8;

CREATE TABLE IF NOT EXISTS `tracker_compressed` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `compression_level` VARCHAR(64) NOT NULL,
  `guild_id` VARCHAR(64) NOT NULL,
  `date_from` DATETIME NOT NULL,
  `date_to` DATETIME NULL,
  `users_online` INT NULL,
  `users_idle` INT NULL,
  `users_dnd` INT NULL,
  `users_offline` INT NULL,
  `users_unknown` INT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARSET = utf8;