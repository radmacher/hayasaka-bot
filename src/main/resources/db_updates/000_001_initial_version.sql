ALTER DATABASE CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `channels`;
CREATE TABLE IF NOT EXISTS `channels` (
  `id`         INT(11) NOT NULL AUTO_INCREMENT,
  `discord_id` VARCHAR(255)     DEFAULT NULL,
  `server_id`  INT(11)          DEFAULT NULL,
  `name`       VARCHAR(255)     DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `channels_discord_id_uindex` (`discord_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `commands`;

CREATE TABLE IF NOT EXISTS `commands` (
  `id`     INT(11)     NOT NULL AUTO_INCREMENT,
  `server` INT(11)              DEFAULT NULL,
  `input`  VARCHAR(50) NOT NULL,
  `output` TEXT        NOT NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `command_cooldown`;

CREATE TABLE IF NOT EXISTS `command_cooldown` (
  `command`     VARCHAR(64) NOT NULL,
  `target_id`   VARCHAR(64) NOT NULL DEFAULT '',
  `target_type` INT(11)     NOT NULL DEFAULT '0',
  `last_time`   INT(21)     NOT NULL,
  PRIMARY KEY (`command`, `target_id`, `target_type`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4	;

DROP TABLE IF EXISTS `command_log`;

CREATE TABLE IF NOT EXISTS `command_log` (
  `id`           INT(11)     NOT NULL AUTO_INCREMENT,
  `user_id`      INT(11)     NOT NULL,
  `guild`        INT(11)              DEFAULT NULL,
  `command`      VARCHAR(64) NOT NULL,
  `args`         TEXT,
  `execute_date` TIMESTAMP   NULL     DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `guild_member`;

CREATE TABLE IF NOT EXISTS `guild_member` (
  `guild_id`  VARCHAR(64)   NOT NULL,
  `user_id`   VARCHAR(64)   NOT NULL,
  `join_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`guild_id`, `user_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS `guild_settings`;

CREATE TABLE IF NOT EXISTS `guild_settings` (
  `guild`  INT(11)      NOT NULL,
  `name`   VARCHAR(255) NOT NULL,
  `config` VARCHAR(255) NOT NULL,
  PRIMARY KEY (`guild`, `name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

DROP TABLE IF EXISTS guilds;

CREATE TABLE IF NOT EXISTS `guilds` (
  `id`         INT(11)      NOT NULL AUTO_INCREMENT,
  `discord_id` VARCHAR(255) NOT NULL,
  `name`       VARCHAR(128)          DEFAULT NULL,
  `owner`      INT(11)      NOT NULL,
  `banned`     INT          NULL,
  `active`     INT          NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `services`;

CREATE TABLE IF NOT EXISTS `services` (
  `id`           INT(11)     NOT NULL AUTO_INCREMENT,
  `name`         VARCHAR(32) NOT NULL,
  `display_name` VARCHAR(64) NOT NULL,
  `description`  TEXT,
  `activated`    INT(11)              DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `subscription_service_name_uindex` (`name`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `service_variables`;

CREATE TABLE IF NOT EXISTS `service_variables` (
  `service_id` INT(11)     NOT NULL,
  `variable`   VARCHAR(64) NOT NULL,
  `value`      VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`service_id`, `variable`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4	;

DROP TABLE IF EXISTS `subscriptions`;

CREATE TABLE IF NOT EXISTS `subscriptions` (
  `server_id`  INT(11) NOT NULL,
  `channel_id` INT(11) NOT NULL,
  `service_id` INT(11) NOT NULL DEFAULT '0',
  `subscribed` INT(11)          DEFAULT NULL,
  PRIMARY KEY (`server_id`, `channel_id`, `service_id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4	;

DROP TABLE IF EXISTS `template_texts`;

CREATE TABLE IF NOT EXISTS `template_texts` (
  `id`        INT(11)      NOT NULL AUTO_INCREMENT,
  `keyphrase` VARCHAR(50)  NOT NULL,
  `text`      VARCHAR(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `txt_search` (`keyphrase`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

DROP TABLE IF EXISTS `users`;

CREATE TABLE IF NOT EXISTS `users` (
  `id`         INT(11)     NOT NULL AUTO_INCREMENT,
  `discord_id` VARCHAR(64) NOT NULL,
  `name`       VARCHAR(128)         DEFAULT NULL,
  `banned`     INT         NULL,
  `commands_used` INT NOT NULL,
  `permission_mask` INT DEFAULT 0 NOT NULL,
  `role_id`    VARCHAR(64) NULL,
  `nickname` VARCHAR (128) NULL,
  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

CREATE TABLE bot_meta (
  meta_name  VARCHAR(32) PRIMARY KEY NOT NULL,
  meta_value VARCHAR(32)
);