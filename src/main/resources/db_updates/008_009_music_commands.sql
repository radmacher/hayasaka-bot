DROP TABLE IF EXISTS music;

CREATE TABLE IF NOT EXISTS `music` (
  `id`            INT(11)     NOT NULL AUTO_INCREMENT,
  `youtubecode`   VARCHAR(32) NOT NULL,
  `filename`      VARCHAR(191) NOT NULL,
  `title`         VARCHAR(128)         DEFAULT NULL,
  `artist`        VARCHAR(128)         DEFAULT NULL,
  `lastplaydate`  INT(11)     NOT NULL DEFAULT '0',
  `banned`        INT(11)     NOT NULL DEFAULT '0',
  `youtube_title` VARCHAR(128)         DEFAULT NULL,
  `play_count`    INT NOT NULL,
  `last_manual_playdate` INT NOT NULL,
  `file_exists` INT DEFAULT 1 NOT NULL,
  `duration` INT DEFAULT 0 NOT NULL,

  PRIMARY KEY (`id`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  AUTO_INCREMENT = 1;

CREATE TABLE bot_playing_on (
  guild_id   VARCHAR(32),
  channel_id VARCHAR(32),
  CONSTRAINT bot_playing_on_pk PRIMARY KEY (guild_id, channel_id)
);

CREATE TABLE music_log (
  id        INT(11) PRIMARY KEY                 NOT NULL AUTO_INCREMENT,
  music_id  INT(11) DEFAULT '0'                 NOT NULL,
  guild_id  INT(11) DEFAULT '0'                 NOT NULL,
  user_id   INT(11),
  play_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);

CREATE INDEX music_log_guild_id_index
  ON music_log (guild_id);
CREATE INDEX music_log_guild_id_music_id_index
  ON music_log (guild_id, music_id);
CREATE INDEX music_log_music_id_index
  ON music_log (music_id);
CREATE INDEX music_filename_unique_index
  ON music (filename);
CREATE INDEX music_youtubecode_unique_index
  ON music (youtubecode);

CREATE TABLE playlist (
  id               INT(11) PRIMARY KEY                 NOT NULL AUTO_INCREMENT,
  title            VARCHAR(255)                        NOT NULL,
  owner_id         INT(11)                             NOT NULL,
  guild_id         INT(11)                             NOT NULL,
  visibility_level INT(11)                             NOT NULL,
  edit_type        INT(11)                             NOT NULL,
  create_date      TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  code VARCHAR(32) DEFAULT 'default' NOT NULL,
  play_type INT(11) NOT NULL
);

CREATE UNIQUE INDEX playlist_owner_id_guild_id_code_uindex
  ON playlist (owner_id, guild_id, code);
CREATE INDEX playlist_owner_id_code_index
  ON playlist (owner_id, code);
CREATE INDEX playlist_guild_id_code_index
  ON playlist (guild_id, code);

CREATE TABLE playlist_item (
  playlist_id INT(11) DEFAULT '0'NOT NULL,
  music_id    INT(11) DEFAULT '0'NOT NULL,
  last_played INT(21),
  CONSTRAINT `PRIMARY` PRIMARY KEY (playlist_id, music_id)
);

CREATE TABLE music_votes
(
  song_id    INT       NOT NULL,
  user_id    INT       NOT NULL,
  vote       INT       NOT NULL,
  created_on TIMESTAMP NOT NULL,
  CONSTRAINT music_votes_song_id_user_id_pk PRIMARY KEY (song_id, user_id)
);