CREATE TABLE bot_events (
  id          INT PRIMARY KEY AUTO_INCREMENT,
  created_on  TIMESTAMP   NOT NULL,
  event_group VARCHAR(32) NOT NULL,
  sub_group   VARCHAR(32),
  data        TEXT
);
ALTER TABLE bot_events
  ADD log_level INT DEFAULT 6 NULL;
ALTER TABLE bot_events
  MODIFY log_level INT(11) NOT NULL DEFAULT '6';
ALTER TABLE `bot_events`
  CHANGE `data` `data` MEDIUMTEXT CHARACTER SET utf8mb4