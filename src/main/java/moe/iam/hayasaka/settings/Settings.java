package moe.iam.hayasaka.settings;

import com.kaaz.configuration.ConfigurationOption;

public class Settings {

    public static final boolean DOCKER_VERSION=false;

    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.71 Safari/537.36";
    //the bot/users ratio for guilds
    public static final double GUILD_MAX_USER_BOT_RATIO = 0.75D;
    //the minimum age of the guild owner's account in days
    public static final long GUILD_OWNER_MIN_ACCOUNT_AGE = 7;
    //if a guild has less users it will be marked as a test guild
    public static final int GUILD_MIN_USERS = 5;
    public static final int MAX_MESSAGE_SIZE = 2000;
    //the default time to delete messages after milliseconds
    public static long DELETE_MESSAGES_AFTER = 120_000;

    public static String TEMPLATE_QUOTE = "%";

    //bot enabled? must be set to true in order to run
    @ConfigurationOption
    public static boolean BOT_ENABLED = false;

    @ConfigurationOption
    public static int MUSIC_MAX_VOLUME = 100;

    @ConfigurationOption
    public static boolean SUBSCRIBE_UNSUB_ON_NOT_FOUND = false;

    @ConfigurationOption
    public static boolean BOT_RESTART_INACTIVE_SHARDS = false;

    @ConfigurationOption
    public static String TOKEN_RIOT_GAMES = "token-here";

    //the website of the bot
    @ConfigurationOption
    public static String BOT_WEBSITE = "i-am.moe";

    @ConfigurationOption
    public static boolean BOT_AUTO_UPDATE = false;

    //display name of the bot
    @ConfigurationOption
    public static String BOT_NAME = "Hayasaka";

    //Bot's own discord server
    @ConfigurationOption
    public static String BOT_GUILD_ID = "295130209085292545";

    //Bot's own channel on its own server
    @ConfigurationOption
    public static String BOT_CHANNEL_ID = "425734858732470312";

    //Bot's error channel id
    @ConfigurationOption
    public static String BOT_ERROR_CHANNEL_ID = "718257375890374667";

    //Bot's status update
    @ConfigurationOption
    public static String BOT_STATUS_CHANNEL_ID = "718257485911162880";

    //token used to login to discord
    @ConfigurationOption
    public static String BOT_TOKEN = "mybottokenhere";

    //prefix for all commands !help etc.
    @ConfigurationOption
    public static boolean BOT_CHATTING_ENABLED = false;

    //default prefix to mark messages as commands
    @ConfigurationOption
    public static String BOT_COMMAND_PREFIX = "+";

    //save the usage of commands?
    @ConfigurationOption
    public static boolean BOT_COMMAND_LOGGING = true;

    //show keyphrases?
    @ConfigurationOption
    public static boolean SHOW_KEYPHRASE = false;

    //Reply to non existing commands?
    @ConfigurationOption
    public static boolean BOT_COMMAND_SHOW_UNKNOWN = false;

    @ConfigurationOption
    public static int MUSIC_MAX_PLAYLIST_SIZE = 50;

    //mysql hostname
    @ConfigurationOption
    public static String DB_HOST = "localhost";

    //mysql port
    @ConfigurationOption
    public static int DB_PORT = 3306;

    //mysql user
    @ConfigurationOption
    public static String DB_USER = "root";

    //mysql password
    @ConfigurationOption
    public static String DB_PASS = "";

    //mysql database name
    @ConfigurationOption
    public static String DB_NAME = "discord";

    @ConfigurationOption
    public static String[] GOOGLE_API_KEY = {"google-api-key-here"};

    @ConfigurationOption
    public static String GIPHY_TOKEN = "dc6zaTOxFJmzC";

    @ConfigurationOption
    public static long CREATOR_ID = 172726364900687872L;

    @ConfigurationOption
    public static String BOT_ENV = "ENV_PLACEHOLDER";

    //if you want to use graylog
    @ConfigurationOption
    public static boolean BOT_GRAYLOG_ACTIVE = false;
    @ConfigurationOption
    public static String BOT_GRAYLOG_HOST = "127.0.0.1";
    @ConfigurationOption
    public static int BOT_GRAYLOG_PORT = 12202;

    @ConfigurationOption
    public static String GENIUS_CLIENT_ACCESS_TOKEN = "your-genius-access-token";

    @ConfigurationOption
    public static boolean ENABLE_TRACKER = false;

    @ConfigurationOption
    public static boolean USE_INFLUXDB_FOR_TRACKER = false;

    @ConfigurationOption
    public static String INFLUX_HOST = "http://influx.db:9999";

    @ConfigurationOption
    public static String INFLUX_TOKEN = "your-influx-token";

    @ConfigurationOption
    public static String INFLUX_BUCKET = "your-influx-bucket-id";

    @ConfigurationOption
    public static String INFLUX_ORG = "your-influx-org-id";

    @ConfigurationOption
    public static String JELLYFIN_URL = "https://jellyfin.do-you-love.me";

    @ConfigurationOption
    public static String JELLYFIN_USER_ID = "your-user-id";

    @ConfigurationOption
    public static String JELLYFIN_API_KEY = "your-api-key";

    @ConfigurationOption
    public static String GITLAB_API_KEY = "your-api-key";

    @ConfigurationOption
    public static String GITLAB_PROJECT_ID = "hayasaka-project-id";

    @ConfigurationOption
    public static String GITLAB_URL = "https://gitlab.com/api/v4";

    @ConfigurationOption
    public static String BOT_CLIENT_ID = "your-client-id";
}
