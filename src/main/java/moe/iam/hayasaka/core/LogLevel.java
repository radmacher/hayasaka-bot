package moe.iam.hayasaka.core;

public enum LogLevel {
    DEBUG,
    INFO,
    WARN,
    FATAL
}
