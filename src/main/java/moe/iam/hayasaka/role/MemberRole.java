package moe.iam.hayasaka.role;

import java.awt.*;

public class MemberRole {
    private final String name;
    private final Color color;

    public MemberRole(String name, Color color) {
        this.name = name;
        this.color = color;
    }


    public String getName() {
        return name;
    }

    public Color getColor() {
        return color;
    }
}
