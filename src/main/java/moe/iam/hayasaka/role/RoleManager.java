package moe.iam.hayasaka.role;

import moe.iam.hayasaka.db.controllers.CGuildMember;
import moe.iam.hayasaka.db.model.OGuildMember;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.handler.GuildSettings;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.Launcher;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.internal.utils.PermissionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class RoleManager {
    private static final ArrayList<MemberShipRole> roles = new ArrayList<>();
    private static final Set<String> roleNames = new HashSet<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(RoleRankings.class);
    private volatile static boolean initialized = false;

    public static void init() {
        if (initialized) {
            return;
        }
        initialized = true;
        roles.add(new MemberShipRole("\uD835\uDCD6\uD835\uDCFE\uD835\uDCEE\uD835\uDCFC\uD835\uDCFD", new Color(0xE67E22), TimeUnit.DAYS.toMillis(125L), true)); //guest
        roles.add(new MemberShipRole("\uD835\uDCE1\uD835\uDCEE\uD835\uDCF0\uD835\uDCFE\uD835\uDCF5\uD835\uDCEA\uD835\uDCFB", new Color(0xE74C3C), TimeUnit.DAYS.toMillis(200L), true)); //regular
        roles.add(new MemberShipRole("\uD835\uDCE1\uD835\uDCEE\uD835\uDCFC\uD835\uDCF2\uD835\uDCED\uD835\uDCEE\uD835\uDCF7\uD835\uDCFD", new Color(0xE91E63), TimeUnit.DAYS.toMillis(365L), true)); //Resident
        roles.add(new MemberShipRole("\uD835\uDCDB\uD835\uDCF8\uD835\uDCFE\uD835\uDCF7\uD835\uDCF0\uD835\uDCEE", new Color(0x42e1ff), TimeUnit.DAYS.toMillis(500L), true)); //Lounge
        for (MemberShipRole role : roles) {
            roleNames.add(role.getName().toLowerCase());
        }
    }

    /**
     * retrieves a list of all membership roles
     *
     * @return list
     */
    public static java.util.List<MemberShipRole> getAllRoles() {
        return roles;
    }

    /**
     * Prefixes the role name based on the guild's setting
     *
     * @param guild the guild
     * @param role  the role
     * @return full name
     */
    public static String getFullName(Guild guild, MemberShipRole role) {
        return getPrefix(guild) + " " + role.getName();
    }

    public static MemberShipRole getHighestRole(Long memberLengthInMilis) {
        for (int i = roles.size() - 1; i >= 0; i--) {
            if (roles.get(i).getMembershipTime() <= memberLengthInMilis) {
                return roles.get(i);
            }
        }
        return roles.get(0);
    }


    /**
     * Attempts to fix create the membership roles for a guild
     *
     * @param guild the guild to create/modify the roles for
     */
    public static void fixForServer(Guild guild) {
        for (int i = roles.size() - 1; i >= 0; i--) {
            fixRole(guild, roles.get(i));
        }
    }

    public static String getPrefix(Guild guild) {
        return GuildSettings.get(guild).getOrDefault(GSetting.USER_TIME_RANKS_PREFIX);
    }

    /**
     * Fixes adds/modifies a membership role to match the settings
     *
     * @param guild the guild to add/modify the role for
     * @param rank  the role to add/modify
     */
    private static void fixRole(Guild guild, MemberShipRole rank) {
        java.util.List<Role> rolesByName = guild.getRolesByName(getFullName(guild, rank), true);
        Role role;
        if (rolesByName.size() > 0) {
            role = rolesByName.get(0);
        } else {
            Role newRole = guild.createRole().complete();
            net.dv8tion.jda.api.managers.RoleManager manager = newRole.getManager();
            manager.setName(getFullName(guild, rank))
                    .setColor(rank.getColor())
                    .setHoisted(rank.isHoisted())
                    .setPermissions(guild.getPublicRole().getPermissions())
                    .revokePermissions(Permission.MESSAGE_MENTION_EVERYONE)
                    .complete();
            return;
        }
        if (!PermissionUtil.canInteract(guild.getSelfMember(), role)) {
            return;
        }
        if (!role.getName().equals(getFullName(guild, rank))) {
            role.getManager().setName(getFullName(guild, rank)).complete();
        }
        if (role.getColor() != rank.getColor()) {
            role.getManager().setColor(rank.getColor()).complete();
        }
        if (role.getPermissions().contains(Permission.MESSAGE_MENTION_EVERYONE)) {
            role.getManager().revokePermissions(Permission.MESSAGE_MENTION_EVERYONE).complete();
        }
    }

    /**
     * checks if a user has the manage roles permission
     *
     * @param guild   the guild to check
     * @param ourUser the user to check for
     * @return has the manage roles premission?
     */
    public static boolean canModifyRoles(Guild guild, User ourUser) {
        return PermissionUtil.checkPermission(guild.getSelfMember(), Permission.MANAGE_ROLES);
    }

    /**
     * deletes the created roles
     *
     * @param guild   the guild to clean up
     * @param ourUser the bot user
     */
    public static void cleanUpRoles(Guild guild, User ourUser) {
        if (!canModifyRoles(guild, ourUser)) {
            return;
        }
        for (Role role : guild.getRoles()) {
            if (role.getName().equals("new role") || role.getName().contains(getPrefix(guild))) {
                role.delete().complete();
            } else if (roleNames.contains(role.getName().toLowerCase())) {
                role.delete().complete();
            }
        }
    }

    /**
     * Attempts to fix create the membership roles for all guilds
     *
     * @param guilds the guilds to fix the roles for
     */
    public static void fixRoles(java.util.List<Guild> guilds) {
        for (Guild guild : guilds) {
            try {
                if (GuildSettings.get(guild) != null) {
                    if (!GuildSettings.get(guild).getBoolValue(GSetting.USER_TIME_RANKS)) {
                        continue;
                    }
                    if (canModifyRoles(guild, guild.getJDA().getSelfUser())) {
                        fixForServer(guild);
                    }
                }
            } catch (Exception e) {
                Launcher.logToDiscord(e, "guild", guild.getId(), "gname", guild.getName());
            }
        }
    }

    /**
     * Asigns the right role to a user based on the Roleranking
     *
     * @param guild the guild
     * @param user  the user
     */
    public static void assignUserRole(DiscordBot bot, Guild guild, User user) {
        java.util.List<Role> roles = guild.getMember(user).getRoles();
        OGuildMember membership = CGuildMember.findBy(guild.getIdLong(), user.getIdLong());
        boolean hasTargetRole = false;
        String prefix = RoleRankings.getPrefix(guild);
        if (membership.joinDate == null) {
            membership.joinDate = new Timestamp(guild.getMember(user).getTimeJoined().toInstant().toEpochMilli());
            CGuildMember.insertOrUpdate(membership);
        }
        MemberShipRole targetRole = RoleRankings.getHighestRole(System.currentTimeMillis() - membership.joinDate.getTime());
        for (Role role : roles) {
            if (role.getName().startsWith(prefix)) {
                if (role.getName().equals(RoleRankings.getFullName(guild, targetRole))) {
                    hasTargetRole = true;
                } else {
                    bot.out.removeRole(user, role);
                }
            }
        }

        if (!hasTargetRole) {
            List<Role> roleList = guild.getRolesByName(RoleRankings.getFullName(guild, targetRole), true);
            if (roleList.size() > 0) {
                bot.out.addRole(user, roleList.get(0));
            }
        }
    }
}
