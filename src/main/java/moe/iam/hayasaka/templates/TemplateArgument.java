package moe.iam.hayasaka.templates;

import moe.iam.hayasaka.settings.Settings;

public enum TemplateArgument {
    ARG("arg1", "First input argument", e -> e.arg[0] != null ? e.arg[0] : ""),
    ARG2("arg2", "Second argument", e -> e.arg[1] != null ? e.arg[1] : ""),
    ARG3("arg3", "Third argument", e -> e.arg[2] != null ? e.arg[2] : ""),
    ARGS("allargs", "All input arguments", e -> e.args != null ? e.args : ""),

    USER("user", "Username", e -> e.user != null ? e.user.getName() : ""),
    USER_MENTION("user-mention", "Mentions user", e -> e.user != null ? e.user.getAsMention() : ""),
    USER_ID("user-id", "User's id", e -> e.user != null ? e.user.getId() : ""),
    USER_DESCRIMINATOR("discrim", "Discriminator of the user", e -> e.user != null ? e.user.getDiscriminator() : ""),

    NICKNAME("nick", "Nickname of user", e -> e.user != null && e.guild != null ? e.guild.getMember(e.user) == null ? e.user.getName() : e.guild.getMember(e.user).getEffectiveName() : ""),
    GUILD("guild", "Guild name", e -> e.guild != null ? e.guild.getName() : ""),
    GUILD_ID("guild-id", "Guild's id", e -> e.guild != null ? e.guild.getId() : ""),
    GUILD_USERS("guild-users", "Sums guild members", e -> e.guild != null ? Integer.toString(e.guild.getMembers().size()) : ""),

    CHANNEL("channel", "Channel name", e -> e.channel != null ? e.channel.getName() : ""),
    CHANNEL_ID("channel-id", "Channel id", e -> e.channel != null ? e.channel.getId() : ""),
    CHANNEL_MENTION("channel-mention", "Mentions channel", e -> e.channel != null ? e.channel.getAsMention() : ""),

    ROLE("role", "Role name", e -> e.role != null ? e.role.getName() : ""),
    ROLE_ID("role-id", "Role's id", e -> e.role != null ? e.role.getId() : ""),
    ROLE_MENTION("role-mention", "mentions the role", e -> e.role != null ? e.role.isMentionable() ? e.role.getAsMention() : e.role.getName() : ""),;

    private final String pattern;
    private final TemplateParser parser;
    private final String description;

    TemplateArgument(String pattern, String description, TemplateParser parser) {
        this.pattern = Settings.TEMPLATE_QUOTE + pattern + Settings.TEMPLATE_QUOTE;
        this.parser = parser;
        this.description = description;
    }

    public String getPattern() {
        return pattern;
    }

    public String parse(TemplateVariables vars) {
        return parser.apply(vars);
    }

    public String getDescription() {
        return description;
    }
}

