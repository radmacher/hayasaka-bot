package moe.iam.hayasaka.templates;

import moe.iam.hayasaka.db.controllers.CGuild;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.handler.GuildSettings;
import moe.iam.hayasaka.settings.Settings;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;

public class Template {
    final private TemplateArgument[] templateArguments;
    final private TemplateArgument[] optionalArgs;
    private String key;

    public Template(TemplateArgument... templateArguments) {
        this(templateArguments, null);
    }

    public Template(TemplateArgument[] requiredArguments, TemplateArgument[] optionalArgs) {
        if (requiredArguments == null) {
            templateArguments = new TemplateArgument[]{};
        } else {
            templateArguments = requiredArguments;
        }
        if (optionalArgs == null) {
            this.optionalArgs = new TemplateArgument[]{};
        } else {
            this.optionalArgs = optionalArgs;
        }
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public TemplateArgument[] getRequiredArguments() {
        return templateArguments;
    }

    public boolean isValidTemplate(String template) {
        if (template == null || template.isEmpty()) {
            return false;
        }
        if (templateArguments.length == 0) {
            return true;
        }
        for (TemplateArgument argument : templateArguments) {
            if (!template.contains(argument.getPattern())) {
                return false;
            }
        }
        return true;
    }

    public String format(Object... vars) {
        return formatFull(0, false, vars);
    }

    public String formatGuild(MessageChannel channel, Object... vars) {
        if (channel.getType().equals(ChannelType.TEXT)) {
            return formatFull(((TextChannel) channel).getGuild().getIdLong(), false, vars);
        }
        return formatFull(0, false, vars);
    }

    public String formatGuild(long guildId, Object... vars) {
        return formatFull(guildId, false, vars);
    }

    public String formatFull(long guildId, boolean forceDebug, Object... vars) {
        boolean showTemplates = forceDebug || Settings.SHOW_KEYPHRASE;
        if (!forceDebug && guildId > 0) {
            showTemplates = GuildSettings.get(guildId).getBoolValue(GSetting.SHOW_TEMPLATES);
        }
        if (templateArguments.length == 0 && optionalArgs.length == 0) {
            if (showTemplates) {
                return "`" + getKey() + "`";
            }
            if (guildId == 0) {
                return TemplateCache.getGlobal(getKey());
            }
            return TemplateCache.getGuild(CGuild.getCachedId(guildId), getKey());
        }
        TemplateVariables env = TemplateVariables.create(vars);
        if (showTemplates) {
            StringBuilder sb = new StringBuilder();
            sb.append("Template: `").append(getKey()).append("`");
            sb.append("\nAvailable arguments:\n```\n");
            if (templateArguments.length > 0) {
                sb.append("Required:\n\n");
                for (TemplateArgument arg : templateArguments) {
                    sb.append(String.format("%-17s -> %s\n", arg.getPattern(), arg.getDescription()));
                    sb.append(String.format("%-17s -> %s\n", " |-> value -> ", arg.parse(env)));
                }
            }
            if (optionalArgs.length > 0) {
                sb.append("\nOptional:\n\n");
                for (TemplateArgument arg : optionalArgs) {
                    sb.append(String.format("%-17s -> %s\n", arg.getPattern(), arg.getDescription()));
                    String var = arg.parse(env);
                    if (!var.isEmpty()) {
                        sb.append(String.format("%-17s -> %s\n", " |-> value -> ", arg.parse(env)));
                    }
                }
            }
            sb.append("```");
            return sb.toString();
        } else {
            String tmp = guildId > 0 ? TemplateCache.getGuild(CGuild.getCachedId(guildId), getKey()) : TemplateCache.getGlobal(getKey());
            for (TemplateArgument arg : templateArguments) {
                tmp = tmp.replace(arg.getPattern(), arg.parse(env));
            }
            for (TemplateArgument arg : optionalArgs) {
                if (tmp.contains(arg.getPattern())) {
                    tmp = tmp.replace(arg.getPattern(), arg.parse(env));
                }
            }
            return tmp;
        }
    }
}
