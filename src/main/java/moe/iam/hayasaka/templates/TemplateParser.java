package moe.iam.hayasaka.templates;

public interface TemplateParser {
    String apply(TemplateVariables env);
}
