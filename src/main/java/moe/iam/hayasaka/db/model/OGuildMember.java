package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

import java.sql.Timestamp;

public class OGuildMember extends AbstractModel {
    public String guildId = "";
    public String userId = "";
    public Timestamp joinDate = null;
}
