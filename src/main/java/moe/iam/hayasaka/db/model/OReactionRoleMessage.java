package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OReactionRoleMessage extends AbstractModel {
    public int id = 0;
    public int reactionRoleKey = 0;
    public String emoji = "";
    public boolean isNormalEmote = true;
    public long roleId = 0L;
}