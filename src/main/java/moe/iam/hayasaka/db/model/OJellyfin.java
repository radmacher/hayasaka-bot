package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

import java.util.Date;

public class OJellyfin extends AbstractModel {
    public int id;
    public String user_id;
    public String title;
    public String url;
    public Date date;
    public int banned;
    public String medium;
}
