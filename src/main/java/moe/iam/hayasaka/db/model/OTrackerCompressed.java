package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

import java.util.Date;

public class OTrackerCompressed extends AbstractModel {
    public long guildId = 0;
    public Date dateFrom = new Date();
    public Date dateTo = new Date();
    public String compressionLevel = "";
    public int usersOnline = 0;
    public int usersIdle = 0;
    public int usersDnD = 0;
    public int usersOffline = 0;
    public int usersUnknown = 0;
}
