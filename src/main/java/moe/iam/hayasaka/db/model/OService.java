package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OService extends AbstractModel {
    public int activated = 0;
    public String description = "";
    public String displayName = "";
    public String name = "";
    public int id = 0;
}
