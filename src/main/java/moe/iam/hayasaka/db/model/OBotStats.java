package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

import java.sql.Timestamp;

public class OBotStats extends AbstractModel {
    public Timestamp createdOn = null;

    public int id = 0;
    public long guildCount = 0;
    public long userCount = 0;
    public long musicCount = 0;
}

