package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OReactionRoleKey extends AbstractModel {
    public int id = 0;
    public String messageKey = "";
    public long channelId = 0L;
    public long messageId = 0L;
    public String message = "";
    public int guildId = 0;
}