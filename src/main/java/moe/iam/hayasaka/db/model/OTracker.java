package moe.iam.hayasaka.db.model;

import com.influxdb.annotations.Column;
import com.influxdb.annotations.Measurement;
import moe.iam.hayasaka.db.AbstractModel;

import java.util.Date;

@Measurement(name = "tracker")
public class OTracker extends AbstractModel {
    public int id = 0;
    @Column(name = "guild_id")
    public long guildId = 0;
    @Column(name = "time")
    public Date date = new Date();
    @Column(name = "online")
    public int usersOnline = 0;
    @Column(name = "idle")
    public int usersIdle = 0;
    @Column(name = "dnd")
    public int usersDnD = 0;
    @Column(name = "offline")
    public int usersOffline = 0;
    @Column(name = "unknown")
    public int usersUnknown = 0;
}
