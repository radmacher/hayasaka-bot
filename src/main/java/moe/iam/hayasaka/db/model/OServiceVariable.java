package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OServiceVariable extends AbstractModel {
    public int serviceId = 0;
    public String variable = "";
    public String value = "";
}
