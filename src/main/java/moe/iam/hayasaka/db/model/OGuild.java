package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OGuild extends AbstractModel {
    public int id = 0;
    public long discord_id = 0L;
    public String name = "";
    public int owner = 0;
    public int active = 0;
    public int banned = 0;

    public boolean isBanned() {
        return banned == 1;
    }
}
