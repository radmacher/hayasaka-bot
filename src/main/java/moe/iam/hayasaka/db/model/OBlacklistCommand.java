package moe.iam.hayasaka.db.model;

import moe.iam.hayasaka.db.AbstractModel;

public class OBlacklistCommand extends AbstractModel {
    public int guildId = 0;
    public String command = "";
    public String channelId = "";
    public boolean blacklisted = false;
}
