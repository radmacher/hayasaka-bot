package moe.iam.hayasaka.db.controllers;

import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OTracker;
import moe.iam.hayasaka.util.TimeUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * data communication with the controllers `tracker`
 */
public class CTracker {

    //TODO: date is NOT the correct data type for this, it will only save DMY in the database and not HMS which are important
    //TODO: compact data after one day, one week and one year to save disk space - maybe add OPTIONAL auto delete?
    //TODO: TRACK MORE DATA: which channels are used the most, how many messages are send each day and by whom (maybe log which roles send the most messages and are the most active in voice channels) etc

    public static OTracker findBy(String discordId) {
        OTracker s = new OTracker();
        try (ResultSet rs = WebDb.get().select(
                "SELECT * FROM tracker WHERE guild_id = ? ORDER BY `tracker`.`id` DESC ", discordId)) {
            if (rs.next()) {
                s = loadRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    private static OTracker loadRecord(ResultSet rs) throws SQLException {
        OTracker s = new OTracker();
        s.id = rs.getInt("id");
        s.guildId = rs.getLong("guild_id");
        s.date = rs.getTimestamp("date");
        s.usersOnline = rs.getInt("users_online");
        s.usersIdle = rs.getInt("users_idle");
        s.usersDnD = rs.getInt("users_dnd");
        s.usersOffline = rs.getInt("users_offline");
        s.usersUnknown = rs.getInt("users_unknown");
        return s;
    }

    public static void insert(OTracker record) {
        try {
            record.guildId = WebDb.get().insert(
                    "INSERT INTO tracker(guild_id, date, users_online, users_idle, users_dnd, users_offline, users_unknown) " +
                            "VALUES (?,?,?,?,?,?,?)",
                    record.guildId, record.date, record.usersOnline, record.usersIdle, record.usersDnD, record.usersOffline, record.usersUnknown);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //FIXME: i think the select statement excludes the past hour instead of the current one, maybe that's a bug or im just too sleepy
    public static void compressOldData(long guildId) {
        List<OTracker> uncompressed = new ArrayList<>();
        List<OTracker> compressed = new ArrayList<>();
        try (
                ResultSet rs = WebDb.get().select(
                        "SELECT * FROM tracker WHERE guild_id = ? AND date NOT LIKE ' " +  TimeUtil.formatMySQLYMDH(TimeUtil.getPreviousHour()) + " %' ORDER BY `tracker`.`id` DESC", guildId)) {
            while (rs.next()) {
                uncompressed.add(loadRecord(rs));
            }
        } catch (SQLException e) {
            Logger.fatal(e);
        }
        List<Integer> toBeDeleted = new ArrayList<>();
        //cycle through all of the uncompressed data, group them into their respective date/hour and compress them
        while (!uncompressed.isEmpty()) {
            List<OTracker> tmp = new ArrayList<>();
            Date currentlyCompressingDate = uncompressed.get(0).date;
            for (OTracker data : uncompressed) {
                if (TimeUtil.formatMySQLYMDH(data.date).equalsIgnoreCase(TimeUtil.formatMySQLYMDH(currentlyCompressingDate))) {
                    toBeDeleted.add(data.id);
                    tmp.add(data);
                }
            }
            compressed.add(compressDataset(tmp, currentlyCompressingDate));
            uncompressed.removeAll(tmp);
        }
        //since the compressed data gets moved into ´tracker_compressed` we have to delete it from `tracker`
        //TODO: it is possible to merge ´tracker_compressed` and `tracker`, but it might create a huge messy table
        for (Integer id : toBeDeleted) {
            try {
                WebDb.get().query(
                        "DELETE FROM tracker WHERE id = ?", id
                );
            } catch (SQLException e) {
                Logger.fatal(e);
            }
        }
        //insert compressed data into ´tracker_compressed` table
        for (OTracker data : compressed) {
            CTrackerCompressed.insertFromDaily(data);
        }
    }

    /*public static void compressDailyData(long guildId, Date dateToCompress) { //SELECT * FROM tracker WHERE guild_id = 295130209085292545 AND date_time LIKE '2020-07-05%' ORDER BY `tracker`.`id` DESC
        List<OTracker> dataToCompress = new ArrayList<>();
        try (ResultSet rs = WebDb.get().select(
                "SELECT * FROM tracker WHERE guild_id = ? AND date_time LIKE '?%' ORDER BY `tracker`.`id` DESC", guildId, TimeUtil.formatMySQLYMD(dateToCompress))) {
            while (rs.next()) {
                dataToCompress.add(loadRecord(rs));
            }
            rs.getStatement().close();
            if (dataToCompress.size() == 1) {
                CTrackerCompressed.insertFromDaily(dataToCompress.get(0));
                WebDb.get().query( //DELETE FROM tracker WHERE guild_id = 295130209085292545 AND date LIKE '2020-07-06%'
                        "DELETE FROM tracker WHERE guild_id = ? AND date LIKE '?%'", guildId, TimeUtil.formatMySQLYMD(dateToCompress)
                );
                return;
            } else if (dataToCompress.size() == 0) {
                return;
            }
            insert(compressDataset(dataToCompress, dateToCompress));
        } catch (Exception e) {
            Logger.fatal(e);
        }
    }*/

    private static OTracker compressDataset(List<OTracker> datasetToCompress, Date date) {
        int online, idle, dnd, offline, unknown;
        online = idle = dnd = offline = unknown = 0;

        if (datasetToCompress.isEmpty())
            return null;

        for (OTracker data : datasetToCompress) {
            if (data.usersOnline >= 0)
                online += data.usersOnline;
            if (data.usersIdle >= 0)
                idle += data.usersIdle;
            if (data.usersDnD >= 0)
                dnd += data.usersDnD;
            if (data.usersOffline >= 0)
                offline += data.usersOffline;
            if (data.usersUnknown >= 0)
                unknown += data.usersUnknown;
        }

        OTracker result = new OTracker();
        result.guildId = datasetToCompress.get(0).guildId;
        result.date = date;
        result.usersOnline = online/datasetToCompress.size();
        result.usersIdle = idle/datasetToCompress.size();
        result.usersDnD = dnd/datasetToCompress.size();
        result.usersOffline = offline/datasetToCompress.size();
        result.usersUnknown = unknown/datasetToCompress.size();
        return result;
    }
}
