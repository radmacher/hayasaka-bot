package moe.iam.hayasaka.db.controllers;

import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OTracker;
import moe.iam.hayasaka.db.model.OTrackerCompressed;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

/**
 * data communication with the controllers `tracker_compressed`
 */
public class CTrackerCompressed {

    /*
        compression level:
            HOUR
            DAY
            WEEK
            MONTH
     */


    //TODO: create better findby method - allow to sort for compression level or date
    public static OTrackerCompressed findBy(String discordId) {
        OTrackerCompressed s = new OTrackerCompressed();
        try (ResultSet rs = WebDb.get().select(
                "SELECT * FROM tracker_compressed WHERE guild_id = ? ORDER BY `tracker`.`id` DESC ", discordId)) {
            if (rs.next()) {
                s = loadRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    private static OTrackerCompressed loadRecord(ResultSet rs) throws SQLException {
        OTrackerCompressed s = new OTrackerCompressed();
        s.guildId = rs.getLong("guild_id");
        s.dateFrom = rs.getTimestamp("date_from");
        s.dateTo = rs.getTimestamp("date_to");
        s.compressionLevel = rs.getString("compression_level");
        s.usersOnline = rs.getInt("users_online");
        s.usersIdle = rs.getInt("users_idle");
        s.usersDnD = rs.getInt("users_dnd");
        s.usersOffline = rs.getInt("users_offline");
        s.usersUnknown = rs.getInt("users_unknown");
        return s;
    }

    public static void insert(OTrackerCompressed record) {
        try {
            record.guildId = WebDb.get().insert(
                    "INSERT INTO tracker_compressed(guild_id, date_from, date_to, compression_level, users_online, users_idle, users_dnd, users_offline, users_unknown) " +
                            "VALUES (?,?,?,?,?,?,?,?,?)",
                    record.guildId, record.dateFrom, record.dateTo, record.compressionLevel, record.usersOnline, record.usersIdle, record.usersDnD, record.usersOffline, record.usersUnknown);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void insertFromDaily(OTracker record) {
        OTrackerCompressed compressed = new OTrackerCompressed();
        compressed.guildId = record.guildId;
        compressed.compressionLevel = "HOUR";
        Calendar cal = Calendar.getInstance();
        cal.setTime(record.date);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        compressed.dateFrom = cal.getTime();
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        compressed.dateTo = cal.getTime();
        compressed.usersOnline = record.usersOnline;
        compressed.usersIdle = record.usersIdle;
        compressed.usersDnD = record.usersDnD;
        compressed.usersOffline = record.usersOffline;
        compressed.usersUnknown = record.usersUnknown;
        insert(compressed);
    }
}
