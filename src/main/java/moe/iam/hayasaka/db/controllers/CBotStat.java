package moe.iam.hayasaka.db.controllers;

import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OBotStats;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

/**
 * data communication with the controllers `bot_events`
 */
public class CBotStat {


    private static OBotStats fillRecord(ResultSet rs) throws SQLException {
        OBotStats s = new OBotStats();
        s.id = rs.getInt("id");
        s.createdOn = rs.getTimestamp("created_on");
        s.guildCount = rs.getLong("event_group");
        s.userCount = rs.getLong("sub_group");
        s.musicCount = rs.getLong("data");
        return s;
    }

    public static void insert(long guildCount, long userCount, long musicCount) {
        OBotStats stats = new OBotStats();
        stats.createdOn = new Timestamp(System.currentTimeMillis());
        stats.guildCount = guildCount;
        stats.userCount = userCount;
        stats.musicCount = musicCount;
        insert(stats);
    }


    public static void insert(OBotStats record) {
        try {
            record.id = WebDb.get().insert(
                    "INSERT INTO bot_stats(created_on,guild_count, user_count, music_count) " +
                            "VALUES (?,?,?,?)",
                    record.createdOn, record.guildCount, record.userCount, record.musicCount
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
