package moe.iam.hayasaka.db.controllers;

import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OGuild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * data communication with the controllers `servers`
 */
public class CGuild {
    private static Map<Long, Integer> guildIdCache = new ConcurrentHashMap<>();
    private static Map<Integer, Long> discordIdCache = new ConcurrentHashMap<>();

    /**
     * Retrieves the internal guild id for {@link MessageChannel} channel
     *
     * @param channel the channel to check
     * @return internal guild-id OR 0 if no guild could be found
     */
    public static int getCachedId(MessageChannel channel) {
        if (channel instanceof TextChannel) {
            return getCachedId(((TextChannel) channel).getGuild().getIdLong());
        }
        return 0;
    }


    public static int getCachedId(long discordId) {
        if (!guildIdCache.containsKey(discordId)) {
            OGuild server = findBy(discordId);
            if (server.id == 0) {
                server.discord_id = discordId;
                server.name = Long.toString(discordId);
                insert(server);
            }
            guildIdCache.put(discordId, server.id);
        }
        return guildIdCache.get(discordId);
    }

    public static String getCachedDiscordId(int id) {
        if (!discordIdCache.containsKey(id)) {
            OGuild server = findById(id);
            if (server.id == 0) {
                return "0";
            }
            discordIdCache.put(id, server.discord_id);
        }
        return Long.toString(discordIdCache.get(id));
    }

    public static List<OGuild> getMostUsedGuildsFor(int userId) {
        List<OGuild> list = new ArrayList<>();
        try (ResultSet rs = WebDb.get().select("SELECT g.id, discord_id, name, owner, active, banned " +
                "FROM command_log l " +
                "JOIN guilds g ON g.id = l.guild " +
                "WHERE l.user_id = ? " +
                "GROUP BY g.id ORDER BY count(l.id) DESC LIMIT 10", userId)) {
            while (rs.next()) {
                list.add(loadRecord(rs));
            }
            rs.getStatement().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static OGuild findBy(long discordId) {
        return findBy(String.valueOf(discordId));
    }

    public static OGuild findBy(String discordId) {
        OGuild s = new OGuild();
        try (ResultSet rs = WebDb.get().select(
                "SELECT id, discord_id, name, owner,active,banned  " +
                        "FROM guilds " +
                        "WHERE discord_id = ? ", discordId)) {
            if (rs.next()) {
                s = loadRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    public static OGuild findById(int id) {
        OGuild s = new OGuild();
        try (ResultSet rs = WebDb.get().select(
                "SELECT id, discord_id, name, owner,active,banned  " +
                        "FROM guilds " +
                        "WHERE id = ? ", id)) {
            if (rs.next()) {
                s = loadRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    public static void update(OGuild record) {
        if (record.id == 0) {
            insert(record);
            return;
        }
        try {
            WebDb.get().query(
                    "UPDATE guilds SET discord_id = ?, name = ?, owner = ?, active = ?, banned = ? " +
                            "WHERE id = ? ",
                    record.discord_id, record.name, record.owner == 0 ? null : record.owner, record.active, record.banned, record.id
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void insert(OGuild record) {
        try {
            record.id = WebDb.get().insert(
                    "INSERT INTO guilds(discord_id, name, owner,active,banned) " +
                            "VALUES (?,?,?,?,?)",
                    record.discord_id, record.name, record.owner == 0 ? null : record.owner, record.active, record.banned);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * retrieves the amount of active guilds
     * note: this value could be higher than the actual active guilds if the bot missed a leave guild event
     *
     * @return active guild count
     */
    public static int getActiveGuildCount() {
        int amount = 0;
        try (ResultSet rs = WebDb.get().select("SELECT count(id) AS amount FROM guilds WHERE active = 1")) {
            while (rs.next()) {
                amount = rs.getInt("amount");
            }
            rs.getStatement().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return amount;
    }

    public static List<OGuild> getBannedGuilds() {
        List<OGuild> list = new ArrayList<>();
        try (ResultSet rs = WebDb.get().select("SELECT * FROM guilds WHERE banned = 1")) {
            while (rs.next()) {
                list.add(loadRecord(rs));
            }
            rs.getStatement().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    private static OGuild loadRecord(ResultSet rs) throws SQLException {
        OGuild s = new OGuild();
        s.id = rs.getInt("id");
        s.discord_id = rs.getLong("discord_id");
        s.name = rs.getString("name");
        s.owner = rs.getInt("owner");
        s.active = rs.getInt("active");
        s.banned = rs.getInt("banned");
        return s;
    }
}
