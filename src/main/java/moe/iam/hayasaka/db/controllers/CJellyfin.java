package moe.iam.hayasaka.db.controllers;

import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OJellyfin;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CJellyfin {

    public static List<OJellyfin> getAllRequests() {
        List<OJellyfin> records = new ArrayList<>();
        try (
                ResultSet rs = WebDb.get().select(
                        "SELECT * FROM jellyfin WHERE banned != 1 ")) {
            while (rs.next()) {
                records.add(loadRecord(rs));
            }
        } catch (SQLException e) {
            Logger.fatal(e);
        }
        return records;
    }

    public static OJellyfin findBy(String animeTitle) {
        OJellyfin s = new OJellyfin();
        try (ResultSet rs = WebDb.get().select(
                "SELECT * FROM jellyfin WHERE title = ? ", animeTitle)) {
            if (rs.next()) {
                s = loadRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    private static OJellyfin loadRecord(ResultSet rs) throws SQLException {
        OJellyfin s = new OJellyfin();
        s.id = rs.getInt("id");
        s.user_id = rs.getString("user_id");
        s.date = rs.getTimestamp("date");
        s.title = rs.getString("title");
        s.url = rs.getString("url");
        s.banned = rs.getInt("banned");
        s.medium = rs.getString("medium");
        return s;
    }

    public static void insert(OJellyfin record) {
        try {
            record.id = WebDb.get().insert(
                    "INSERT INTO jellyfin(user_id, date, title, url, banned, medium) " +
                            "VALUES (?,?,?,?,?,?)",
                    record.user_id, record.date, record.title, record.url, record.banned, record.medium);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void update(OJellyfin record) {
        if (record.id == 0) {
            insert(record);
            return;
        }
        try {
            WebDb.get().query(
                    "UPDATE jellyfin SET banned = ? " +
                            "WHERE id = ? ",
                    record.banned, record.id
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
