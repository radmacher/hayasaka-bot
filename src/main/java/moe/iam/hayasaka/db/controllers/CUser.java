package moe.iam.hayasaka.db.controllers;

import emoji4j.EmojiUtils;
import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.model.OUser;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * data communication with the controllers `users`
 */
public class CUser {

    //TODO: REMOVE CURRENCY STUFF

    private static Map<Long, Integer> userCache = new ConcurrentHashMap<>();
    private static Map<Integer, Long> discordCache = new ConcurrentHashMap<>();

    public static int getCachedId(long discordId) {
        return getCachedId(discordId, String.valueOf(discordId));
    }

    public static int getCachedId(long discordId, String username) {
        if (!userCache.containsKey(discordId)) {
            OUser user = findBy(discordId);
            if (user.id == 0) {
                user.discord_id = String.valueOf(discordId);
                user.name = username;
                insert(user);
            }
            if (user.name == null || user.name.isEmpty() || user.name.equals(username)) {
                user.name = EmojiUtils.shortCodify(username);
                update(user);
            }
            userCache.put(discordId, user.id);
        }
        return userCache.get(discordId);
    }

    public static long getCachedDiscordId(int userId) {
        if (!discordCache.containsKey(userId)) {
            OUser user = findById(userId);
            if (user.id == 0) {
                return 0L;
            }
            discordCache.put(userId, Long.parseLong(user.discord_id));
        }
        return discordCache.get(userId);
    }


    public static OUser findBy(long discordId) {
        OUser s = new OUser();
        try (ResultSet rs = WebDb.get().select(
                "SELECT *  " +
                        "FROM users " +
                        "WHERE discord_id = ? ", discordId)) {
            if (rs.next()) {
                s = fillRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    public static OUser findById(int internalId) {
        OUser s = new OUser();
        try (ResultSet rs = WebDb.get().select(
                "SELECT *  " +
                        "FROM users " +
                        "WHERE id = ? ", internalId)) {
            if (rs.next()) {
                s = fillRecord(rs);
            }
            rs.getStatement().close();
        } catch (Exception e) {
            Logger.fatal(e);
        }
        return s;
    }

    private static OUser fillRecord(ResultSet rs) throws SQLException {
        OUser s = new OUser();
        s.id = rs.getInt("id");
        s.discord_id = rs.getString("discord_id");
        s.name = rs.getString("name");
        s.commandsUsed = rs.getInt("commands_used");
        s.banned = rs.getInt("banned");
        s.setPermission(rs.getInt("permission_mask"));
        s.nickname = rs.getString("nickname");
        s.roleId = rs.getString("role_id");
        s.time_online = rs.getDate("time_online");
        s.last_seen = rs.getDate("last_seen");
        return s;
    }

    public static List<OUser> getBannedUsers() {
        List<OUser> list = new ArrayList<>();
        try (ResultSet rs = WebDb.get().select("SELECT * FROM users WHERE banned = 1")) {
            while (rs.next()) {
                list.add(fillRecord(rs));
            }
            rs.getStatement().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public static void registerCommandUse(int userId) {
        try {
            WebDb.get().query(
                    "UPDATE users SET  commands_used = commands_used + 1 " +
                            "WHERE id = ? ",
                    userId
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addMinuteOnline(long userId) {
        OUser user = findBy(userId);
        if (user.discord_id.equals(userId + "")) {
            if (user.time_online != null) {
                user.time_online.setTime(user.time_online.getTime() + (60 * 1000)); //add one minute to time_online
            } else {
                user.time_online = new Date(60 * 1000);
            }
            user.last_seen = new Date(System.currentTimeMillis());
            update(user);
        }
    }

    public static void update(OUser record) {
        if (record.id == 0) {
            insert(record);
            return;
        }
        try {
            WebDb.get().query(
                    "UPDATE users SET discord_id = ?, name = ?, banned = ?, commands_used = ?, permission_mask = ?, role_id = ?, nickname = ?, last_seen = ?, time_online = ? " +
                            "WHERE id = ? ",
                    record.discord_id, record.name, record.banned, record.commandsUsed, record.getEncodedPermissions(), record.roleId, record.nickname, record.last_seen, record.time_online, record.id
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void insert(OUser record) {
        try {
            record.id = WebDb.get().insert(
                    "INSERT INTO users(discord_id,commands_used, name, banned, permission_mask, role_id, nickname, last_seen, time_online) " +
                            "VALUES (?,?,?,?,?,?,?,?,?)",
                    record.discord_id, record.commandsUsed, record.name, record.banned, record.getEncodedPermissions(), record.roleId, record.nickname, record.last_seen, record.time_online);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void addBannedUserIds(HashSet<Long> bannedUsers) {
        try (ResultSet rs = WebDb.get().select("SELECT * FROM users WHERE banned = 1")) {
            while (rs.next()) {
                bannedUsers.add(rs.getLong("discord_id"));
            }
            rs.getStatement().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
