package moe.iam.hayasaka.games.meta;

public enum GameState {
    INITIALIZING, READY, IN_PROGRESS, OVER;
}
