package moe.iam.hayasaka.games.meta;

public enum GamePlayer {
    FREE, PLAYER1, PLAYER2;
}