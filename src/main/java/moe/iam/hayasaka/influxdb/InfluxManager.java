package moe.iam.hayasaka.influxdb;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApi;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import com.influxdb.exceptions.NotFoundException;
import moe.iam.hayasaka.core.ExitCode;
import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.model.OTracker;
import moe.iam.hayasaka.main.Launcher;
import moe.iam.hayasaka.settings.Settings;

public class InfluxManager {

    private static InfluxDBClient client;
    public static final boolean enabled = Settings.USE_INFLUXDB_FOR_TRACKER;
    private static final String host = Settings.INFLUX_HOST;
    private static final String token = Settings.INFLUX_TOKEN;
    private static final String bucket = Settings.INFLUX_BUCKET;
    private static final String org = Settings.INFLUX_ORG;

    public static void init() {
        if (enabled) {
                client = InfluxDBClientFactory.create(host, token.toCharArray());
            if (!isConnected()) {
                Logger.fatal("You set Influx to enabled in your config, but I could not connect using the given credentials. Either disable it or edit your credentials.");
                Launcher.stop(ExitCode.SHITTY_CONFIG);
            }
        }
    }

    public static void addData(OTracker record) {
        if (client == null)
            return;
        Point point = Point.measurement("tracker")
                .time(record.date.getTime(), WritePrecision.MS)
                .addField("guild_id", record.guildId)
                .addField("online", record.usersOnline)
                .addField("idle", record.usersIdle)
                .addField("dnd", record.usersDnD)
                .addField("offline", record.usersOffline)
                .addField("unknown", record.usersUnknown);
        try (WriteApi writeApi = client.getWriteApi()) {
            writeApi.writePoint(bucket, org, point);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
    }

    /*public static List<OTracker> getData() {
        QueryResult queryResult = influxDB.query(new Query("SELECT * from tracker", Settings.INFLUX_DB_NAME));
        InfluxDBResultMapper resultMapper = new InfluxDBResultMapper();
        return resultMapper.toPOJO(queryResult, OTracker.class);
    }*/

    /*public static boolean isConnected() {
        Pong response = influxDB.ping();
        return !response.getVersion().equalsIgnoreCase("unknown");
    }*/

    public static boolean isEnabled() {
        return enabled;
    }

    public static boolean isConnected() {
        return client.health().getStatus().getValue().equals("pass");
    }
}
