package moe.iam.hayasaka.main;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.core.ExitCode;
import moe.iam.hayasaka.handler.CommandHandler;
import moe.iam.hayasaka.handler.GameHandler;
import moe.iam.hayasaka.handler.SecurityHandler;
import moe.iam.hayasaka.role.RoleRankings;
import moe.iam.hayasaka.settings.Settings;
import moe.iam.hayasaka.templates.TemplateCache;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.Misc;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.security.auth.login.LoginException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLongArray;

public class BotContainer {
    public static final Logger LOGGER = LogManager.getLogger(DiscordBot.class);
    private final int numShards;
    private final DiscordBot[] shards;
    private final AtomicBoolean statusLocked = new AtomicBoolean(false);
    private final AtomicInteger numGuilds;
    private final AtomicLongArray lastActions;
    private final ScheduledExecutorService scheduler;
    private volatile boolean allShardsReady = false;
    private volatile boolean terminationRequested = false;
    private volatile ExitCode rebootReason = ExitCode.UNKNOWN;


    public BotContainer(int numGuilds) throws LoginException, InterruptedException, RateLimitedException {
        scheduler = Executors.newScheduledThreadPool(3);
        this.numGuilds = new AtomicInteger(numGuilds);
        this.numShards = getRecommendedShards();
        shards = new DiscordBot[numShards];
        lastActions = new AtomicLongArray(numShards);
        initHandlers();
        initShards();
    }

    /**
     * restarts a shard
     *
     * @param shardId the shard to restart
     * @return true if it restarted
     */
    public synchronized boolean tryRestartingShard(int shardId) {
        try {
            restartShard(shardId);
        } catch (InterruptedException | LoginException | RateLimitedException e) {
            BotContainer.LOGGER.error("rebootshard failed", e);
            Launcher.logToDiscord(e, "shard-restart", "failed", "shard-id", shardId);
            return false;
        }
        return true;
    }

    /**
     * Schedule the a task somewhere in the future
     *
     * @param task     the task
     * @param delay    the delay
     * @param timeUnit unit type of delay
     */
    public void schedule(Runnable task, Long delay, TimeUnit timeUnit) {
        scheduler.schedule(task, delay, timeUnit);
    }

    /**
     * schedule a repeating task
     *
     * @param task        the taks
     * @param startDelay  delay before starting the first iteration
     * @param repeatDelay delay between consecutive executions
     */
    public ScheduledFuture<?> scheduleRepeat(Runnable task, long startDelay, long repeatDelay) {
        return scheduler.scheduleWithFixedDelay(task, startDelay, repeatDelay, TimeUnit.MILLISECONDS);
    }

    /**
     * restarts a shard
     *
     * @param shardId the shard to restart
     * @throws InterruptedException
     * @throws LoginException
     * @throws RateLimitedException
     */
    public synchronized void restartShard(int shardId) throws InterruptedException, LoginException, RateLimitedException {
        System.out.println("shutting down shard " + shardId);
        shards[shardId].getJda().shutdown();
        System.out.println("SHUT DOWN SHARD " + shardId);
        schedule(() -> {
            while (true) {
                try {
                    shards[shardId].restartJDA();
                    break;
                } catch (LoginException | InterruptedException | RateLimitedException e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(5_000L);
                    } catch (InterruptedException ignored) {
                    }
                }
            }
            reportError(String.format("Quick, shard `%02d` is on %s, where are the %s'? Restarting the shard, off we go %s!",
                    shardId, Emojibet.FIRE, Emojibet.FIRE_TRUCK, Emojibet.ROCKET));
        }, 5L, TimeUnit.SECONDS);
    }

    public void setLastAction(int shard, long timestamp) {
        lastActions.set(shard, timestamp);
    }

    public long getLastAction(int shard) {
        return lastActions.get(shard);
    }

    /**
     * Request that the bot exits
     *
     * @param reason the reason
     */
    public synchronized void requestExit(ExitCode reason) {
        if (!terminationRequested) {
            terminationRequested = true;
            rebootReason = reason;
        }
    }

    /**
     *
     */
    public synchronized void firmRequestExit(ExitCode reason) {
        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(300_000);// 5 minutes
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.exit(reason.getCode());
        }, "Firm-Request-Exit-Thread");
        thread.setDaemon(true);
        thread.start();
        requestExit(reason);
    }

    /**
     * report an error to the configured error channel
     *
     * @param error   the Exception
     * @param details extra details about the error
     */

    public void reportError(Throwable error, Object... details) {
        StringBuilder errorMessage = new StringBuilder("I've encountered a **" + error.getClass().getName() + "**\n");
        if (error.getMessage() != null) {
            errorMessage.append("Message:\n");
            errorMessage.append(error.getMessage()).append("\n\n");
        }
        StringBuilder stack = new StringBuilder();
        int maxTrace = 10;
        StackTraceElement[] stackTrace1 = error.getStackTrace();
        for (int i = 0; i < stackTrace1.length; i++) {
            StackTraceElement stackTrace = stackTrace1[i];
            stack.append(stackTrace.toString()).append("\n");
            if (i > maxTrace) {
                break;
            }
        }
        if (details.length > 0) {
            errorMessage.append("Extra information:\n");
            for (int i = 1; i < details.length; i += 2) {
                if (details[i] != null) {
                    errorMessage.append(details[i - 1]).append(" = ").append(details[i]).append("\n");
                } else if (details[i - 1] != null) {
                    errorMessage.append(details[i - 1]);
                }
            }
            errorMessage.append("\n\n");
        }
        errorMessage.append("Accompanied stacktrace:\n").append(Misc.makeTable(stack.toString())).append("\n");
        reportError(errorMessage.toString());
    }

    public void reportError(String message) {
        DiscordBot shard = getShardFor(Settings.BOT_GUILD_ID);
        Guild guild = shard.getJda().getGuildById(Settings.BOT_GUILD_ID);
        if (guild == null) {
            LOGGER.warn("Can't find BOT_GUILD_ID " + Settings.BOT_GUILD_ID);
            return;
        }
        TextChannel channel = guild.getTextChannelById(Settings.BOT_ERROR_CHANNEL_ID);
        if (channel == null) {
            LOGGER.warn("Can't find BOT_ERROR_CHANNEL_ID " + Settings.BOT_ERROR_CHANNEL_ID);
            return;
        }
        shard.queue.add(channel.sendMessage(message.length() > Settings.MAX_MESSAGE_SIZE ? message.substring(0, Settings.MAX_MESSAGE_SIZE - 1) : message));
    }

    public void reportStatus(int shardId, JDA.Status oldStatus, JDA.Status status) {
        DiscordBot shard = getShardFor(Settings.BOT_GUILD_ID);
        if (shard == null || shard.getJda() == null) {
            return;
        }
        Guild guild = shard.getJda().getGuildById(Settings.BOT_GUILD_ID);
        if (guild == null) {
            LOGGER.warn("Can't find BOT_GUILD_ID " + Settings.BOT_GUILD_ID);
            return;
        }
        TextChannel channel = guild.getTextChannelById(Settings.BOT_STATUS_CHANNEL_ID);
        if (channel == null) {
            LOGGER.warn("Can't find BOT_STATUS_CHANNEL_ID " + Settings.BOT_STATUS_CHANNEL_ID);
            return;
        }
        if (channel.getJDA().getStatus() == JDA.Status.CONNECTED) {
            int length = 1 + (int) Math.floor(Math.log10(shards.length));
            shard.queue.add(channel.sendMessage(
                    String.format(Emojibet.SHARD_ICON + " `%0" + length + "d/%0" + length + "d` | ~~%s~~ -> %s",
                            shardId, shards.length, oldStatus.toString(), status.toString())));
        }
    }

    /**
     * update the numguilds so that we can check if we need an extra shard
     */
    public void guildJoined() {
        int suggestedShards = 1 + ((numGuilds.incrementAndGet() + 500) / 2000);
        if (suggestedShards > numShards) {
            terminationRequested = true;
            rebootReason = ExitCode.NEED_MORE_SHARDS;
        }
    }

    /**
     * Retrieves the shard recommendation from discord
     *
     * @return recommended shard count
     */
    public int getRecommendedShards() {
        try {
            HttpResponse<JsonNode> request = Unirest.get("https://discordapp.com/api/gateway/bot")
                    .header("Authorization", "Bot " + Settings.BOT_TOKEN)
                    .header("Content-Type", "application/json")
                    .asJson();
            return Integer.parseInt(request.getBody().getObject().get("shards").toString());
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return 1;
    }

    /**
     * {@link BotContainer#guildJoined()}
     */
    public void guildLeft() {
        numGuilds.decrementAndGet();
    }

    public DiscordBot[] getShards() {
        return shards;
    }

    /**
     * {@link BotContainer#getShardFor(long)}
     */
    public DiscordBot getShardFor(String discordGuildId) {
        if (numShards == 1) {
            return shards[0];
        }
        return getShardFor(Long.parseLong(discordGuildId));
    }

    /**
     * Retrieves the right shard for the guildId
     *
     * @param discordGuildId the discord guild id
     * @return the instance responsible for the guild
     */
    public DiscordBot getShardFor(long discordGuildId) {
        if (numShards == 1) {
            return shards[0];
        }
        return shards[calcShardId(discordGuildId)];
    }

    /**
     * calculate to which shard the guild goes to
     *
     * @param discordGuildId discord guild id
     * @return shard number
     */
    public int calcShardId(long discordGuildId) {
        return (int) ((discordGuildId >> 22) % numShards);
    }

    /**
     * creates a new instance for each shard
     *
     * @throws LoginException       can't log in
     * @throws InterruptedException ¯\_(ツ)_/¯
     */
    private void initShards() throws LoginException, InterruptedException, RateLimitedException {
        for (int i = 0; i < shards.length; i++) {
            LOGGER.info("Starting shard #{} of {}", i, shards.length);
            shards[i] = new DiscordBot(i, shards.length, this);
            Thread.sleep(5_000L);
        }
        for (DiscordBot shard : shards) {
            setLastAction(shard.getShardId(), System.currentTimeMillis());
        }
    }

    /**
     * After the bot is ready to go; reconnect to the voicechannels and start playing where it left off
     */
    private void onAllShardsReady() {
        TemplateCache.initGuildTemplates(this);
        System.out.println("DONE LOADING TEMPLATES");
    }

    private void initHandlers() {
        CommandHandler.initialize();
        GameHandler.initialize();
        SecurityHandler.initialize();
        Templates.init();
        SecurityHandler.initialize();
        RoleRankings.init();
    }

    /**
     * checks if all shards are ready
     *
     * @return all shards ready
     */
    public boolean allShardsReady() {
        if (allShardsReady) {
            return allShardsReady;
        }
        for (DiscordBot shard : shards) {
            if (shard == null || !shard.isReady()) {
                return false;
            }
        }
        allShardsReady = true;
        onAllShardsReady();
        return true;
    }

    public boolean isTerminationRequested() {
        return terminationRequested;
    }

    public ExitCode getRebootReason() {
        return rebootReason;
    }

    /**
     * Check if the bot's status is locked
     * If its locked, the bot will not change its status
     *
     * @return locked?
     */
    public boolean isStatusLocked() {
        return statusLocked.get();
    }

    /**
     * Lock/unlock the bot's status
     *
     * @param locked lock?
     */
    public void setStatusLocked(boolean locked) {
        statusLocked.set(locked);
    }
}
