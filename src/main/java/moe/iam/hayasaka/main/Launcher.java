package moe.iam.hayasaka.main;

import com.kaaz.configuration.ConfigurationBuilder;
import moe.iam.hayasaka.core.ExitCode;
import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.db.DbUpdate;
import moe.iam.hayasaka.db.WebDb;
import moe.iam.hayasaka.db.controllers.CBotPlayingOn;
import moe.iam.hayasaka.db.controllers.CGuild;
import moe.iam.hayasaka.db.controllers.CMusic;
import moe.iam.hayasaka.db.model.OMusic;
import moe.iam.hayasaka.influxdb.InfluxManager;
import moe.iam.hayasaka.settings.Settings;
import moe.iam.hayasaka.threads.GrayLogThread;
import moe.iam.hayasaka.threads.ServiceHandlerThread;
import moe.iam.hayasaka.util.YTUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.managers.AudioManager;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Properties;

public class Launcher {
    public volatile static boolean isBeingKilled = false;
    private static GrayLogThread GRAYLOG;
    private static BotContainer botContainer = null;
    private static ProgramVersion version = new ProgramVersion("maven-version-number");


    /**
     * log all the things!
     *
     * @param message the log message
     * @param type    the category of the log message
     * @param subtype the subcategory of a logmessage
     * @param args    optional extra arguments
     */
    public static void log(String message, String type, String subtype, Object... args) {
        if (GRAYLOG != null && Settings.BOT_GRAYLOG_ACTIVE) {
            GRAYLOG.log(message, type, subtype, args);
        }
    }

    public static void logToDiscord(Throwable e, Object... args) {
        if (botContainer != null) {
            botContainer.reportError(e, args);
        }
    }

    public static void logToDiscord(String msg) {
        if (botContainer != null) {
            botContainer.reportError(msg);
        }
    }

    public static ProgramVersion getVersion() {
        return version;
    }

    public static void main(String[] args) throws Exception {
        //System.out.println("23:12");
        if (args.length > 0 && args[0].equalsIgnoreCase("DOCKER")) {
            File setttingsFolder = new File("/usr/settings/tmp.tmp");
            if (setttingsFolder.mkdirs())
                setttingsFolder.delete();
            new ConfigurationBuilder(Settings.class, new File("/usr/settings/settings.cfg")).build(true);
        } else {
            new ConfigurationBuilder(Settings.class, new File("settings.cfg")).build(true);
        }
        WebDb.init();
        InfluxManager.init();
        Launcher.init();
        if (Settings.BOT_ENABLED) {
            Runtime.getRuntime().addShutdownHook(new Thread(Launcher::shutdownHook));
            try {
                botContainer = new BotContainer((CGuild.getActiveGuildCount()));
                Thread serviceHandler = new ServiceHandlerThread(botContainer);
                serviceHandler.start();
            } catch (Exception e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
                Launcher.stop(ExitCode.SHITTY_CONFIG, e);
            }
        } else {
            Logger.fatal("Bot not enabled, enable it in the config. You can do this by setting bot_enabled=true");
            Launcher.stop(ExitCode.SHITTY_CONFIG);
        }
    }

    private static void init() throws IOException, InterruptedException, SQLException {
        Properties props = new Properties();
        props.load(Launcher.class.getClassLoader().getResourceAsStream("project.properties"));
        Launcher.version = ProgramVersion.fromString(String.valueOf(props.getOrDefault("version", "1")));
        System.out.println(String.format("Started Hayasaka with version %s", Launcher.version));
        DbUpdate dbUpdate = new DbUpdate(WebDb.get());
        dbUpdate.updateToCurrent();
        Launcher.GRAYLOG = new GrayLogThread();
        Launcher.GRAYLOG.start();
    }

    /**
     * Stop the bot!
     *
     * @param reason why!?
     */
    public static void stop(ExitCode reason) {
        stop(reason, null);
    }

    public static void stop(ExitCode reason, Exception e) {
        if (isBeingKilled) {
            return;
        }
        isBeingKilled = true;
        DiscordBot.LOGGER.error("Exiting because: " + reason);
        if (e != null) {
            System.out.println(e);
        }
        System.exit(reason.getCode());
    }

    /**
     * shutdown hook, closing connections
     *
     */
    private static void shutdownHook() {
        if (botContainer != null) {
            for (DiscordBot discordBot : botContainer.getShards()) {
                for (Guild guild : discordBot.getJda().getGuilds()) {
                    AudioManager audio = guild.getAudioManager();
                    if (audio.isConnected()) {
                        CBotPlayingOn.insert(guild.getId(), audio.getConnectedChannel().getId());
                    }
                }
                discordBot.getJda().shutdown();
            }
        }
    }

    /**
     * helper function, retrieves youtubeTitle for mp3 files which contain youtube videocode as filename
     */
    public static void fixExistingYoutubeFiles() {
        for (String file : new String[]{}) {
            System.out.println(file);
            String videocode = file.replace(".mp3", "");
            OMusic rec = CMusic.findByYoutubeId(videocode);
            rec.youtubeTitle = YTUtil.getTitleFromPage(videocode);
            rec.youtubecode = videocode;
            rec.filename = videocode + ".mp3";
            CMusic.update(rec);
        }
    }
}