package moe.iam.hayasaka.guildsettings;

import moe.iam.hayasaka.guildsettings.types.*;
import moe.iam.hayasaka.settings.Settings;

public class GuildSettingType {
    public static final IGuildSettingType INTERNAL = new NoSettingType();
    public static final IGuildSettingType TOGGLE = new BooleanSettingType();
    public static final IGuildSettingType PERCENTAGE = new NumberBetweenSettingType(0, 100);
    public static final IGuildSettingType VOLUME = new NumberBetweenSettingType(0, Settings.MUSIC_MAX_VOLUME);
    public static final IGuildSettingType TEXT_CHANNEL_OPTIONAL = new TextChannelSettingType(true);
    public static final IGuildSettingType TEXT_CHANNEL_MANDATORY = new TextChannelSettingType(false);
    public static final IGuildSettingType ROLE_OPTIONAL = new RoleSettingType(true);
    public static final IGuildSettingType ROLE_MANDATORY = new RoleSettingType(false);
    public static final IGuildSettingType VOICE_CHANNEL_OPTIONAL = new VoiceChannelSettingType(true);
    public static final IGuildSettingType VOICE_CHANNEL_MANDATORY = new VoiceChannelSettingType(false);
}
