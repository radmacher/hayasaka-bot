package moe.iam.hayasaka.guildsettings;

public enum GSettingTag {
    META, LOGGING, ROLE, MODERATION, CHANNEL, MUSIC, ADMIN, INTERNAL, COMMAND, DEBUG
}