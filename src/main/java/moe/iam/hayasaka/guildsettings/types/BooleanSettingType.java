package moe.iam.hayasaka.guildsettings.types;

import moe.iam.hayasaka.guildsettings.IGuildSettingType;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.Misc;
import net.dv8tion.jda.api.entities.Guild;

/**
 * boolean settings type
 * {@link Misc#isFuzzyTrue(String)}   yes
 * {@link Misc#isFuzzyFalse(String)}  no
 */
public class BooleanSettingType implements IGuildSettingType {
    @Override
    public String typeName() {
        return "toggle";
    }

    @Override
    public boolean validate(Guild guild, String value) {
        return value != null && (Misc.isFuzzyTrue(value) || Misc.isFuzzyFalse(value));
    }

    @Override
    public String fromInput(Guild guild, String value) {
        return Misc.isFuzzyTrue(value) ? "true" : "false";
    }

    @Override
    public String toDisplay(Guild guild, String value) {
        return "true".equals(value) ? Emojibet.OKE_SIGN : Emojibet.X;
    }
}
