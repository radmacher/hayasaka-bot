package moe.iam.hayasaka.event;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.events.GenericEvent;
import net.dv8tion.jda.api.exceptions.PermissionException;
import net.dv8tion.jda.api.hooks.EventListener;
import net.dv8tion.jda.api.hooks.IEventManager;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class JDAEventManager implements IEventManager {

    private final DiscordBot bot;
    private final ThreadPoolExecutor threadExecutor;
    private List<Object> listeners = new LinkedList<>();

    public JDAEventManager(DiscordBot bot) {
        ThreadFactoryBuilder threadBuilder = new ThreadFactoryBuilder();
        threadBuilder.setNameFormat(String.format("shard-%02d-command-%%d", bot.getShardId()));
        this.threadExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool(threadBuilder.build());
        this.bot = bot;
    }

    @Override
    public void register(Object listener) {
        if (!(listener instanceof EventListener)) {
            throw new IllegalArgumentException("Listener must implement EventListener");
        }
        listeners.add(listener);
    }

    @Override
    public void unregister(Object listener) {
        listeners.remove(listener);
    }

    @Override
    public void handle(GenericEvent event) {
        threadExecutor.submit(() -> {
            bot.getContainer().setLastAction(event.getJDA().getShardInfo() == null ? 0 : event.getJDA().getShardInfo().getShardId(), System.currentTimeMillis());
            bot.updateJda(event.getJDA());
            if (!(event.getJDA().getStatus() == JDA.Status.CONNECTED)) {
                return;
            }
            List<Object> listenerCopy = new LinkedList<>(listeners);
            for (Object listener : listenerCopy) {
                try {
                    ((EventListener) listener).onEvent(event);
                } catch (PermissionException throwable) {
                    Logger.fatal("unchecked permission error!");
                    Logger.fatal(throwable);
                } catch (Throwable throwable) {
                    Logger.fatal(throwable);
                    bot.getContainer().reportError(throwable);
                }
            }
        });
    }

    @Override
    public List<Object> getRegisteredListeners() {
        return this.listeners;
    }

}
