package moe.iam.hayasaka.event;

import emoji4j.EmojiUtils;
import moe.iam.hayasaka.command.misc.MoveMeCommand;
import moe.iam.hayasaka.db.controllers.CBotEvent;
import moe.iam.hayasaka.db.controllers.CGuild;
import moe.iam.hayasaka.db.controllers.CGuildMember;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.model.OGuild;
import moe.iam.hayasaka.db.model.OGuildMember;
import moe.iam.hayasaka.db.model.OUser;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.handler.GuildSettings;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.GuildCheckResult;
import moe.iam.hayasaka.main.Launcher;
import moe.iam.hayasaka.role.RoleRankings;
import moe.iam.hayasaka.settings.Settings;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.DisUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.api.events.DisconnectEvent;
import net.dv8tion.jda.api.events.ReconnectedEvent;
import net.dv8tion.jda.api.events.ResumedEvent;
import net.dv8tion.jda.api.events.StatusChangeEvent;
import net.dv8tion.jda.api.events.guild.GuildBanEvent;
import net.dv8tion.jda.api.events.guild.GuildJoinEvent;
import net.dv8tion.jda.api.events.guild.GuildLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.api.events.guild.member.GuildMemberLeaveEvent;
import net.dv8tion.jda.api.events.guild.member.update.GuildMemberUpdateNicknameEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceGuildDeafenEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceLeaveEvent;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceMoveEvent;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.GenericMessageReactionEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.awt.*;
import java.sql.Timestamp;
import java.util.concurrent.TimeUnit;

public class JDAEvents extends ListenerAdapter {
    private final DiscordBot discordBot;

    public JDAEvents(DiscordBot bot) {
        this.discordBot = bot;
    }

    public void onDisconnect(DisconnectEvent event) {
        DiscordBot.LOGGER.info("[event] DISCONNECTED! ");
    }

    @Override
    public void onStatusChange(StatusChangeEvent event) {
        discordBot.getContainer().reportStatus(event.getJDA().getShardInfo() != null ? event.getJDA().getShardInfo().getShardId() : 0, event.getOldStatus(), event.getNewStatus());
    }

    @Override
    public void onResume(ResumedEvent event) {
    }

    @Override
    public void onReconnect(ReconnectedEvent event) {
        discordBot.getContainer().reportError(String.format("[RECONNECT] \\#%02d with a different JDA", discordBot.getShardId()));
    }

    public void onGuildJoin(GuildJoinEvent event) {
        Guild guild = event.getGuild();
        User owner = guild.getOwner().getUser();
        OUser user = CUser.findBy(owner.getIdLong());
        user.discord_id = owner.getId();
        user.name = EmojiUtils.shortCodify(owner.getName());
        CUser.update(user);
        OGuild dbGuild = CGuild.findBy(guild.getId());
        dbGuild.discord_id = guild.getIdLong();
        dbGuild.name = EmojiUtils.shortCodify(guild.getName());
        dbGuild.owner = user.id;
        if (dbGuild.id == 0) {
            CGuild.insert(dbGuild);
        }
        if (dbGuild.isBanned()) {
            discordBot.queue.add(guild.leave());
            return;
        }
        discordBot.loadGuild(guild);
        String cmdPre = GuildSettings.get(guild).getOrDefault(GSetting.COMMAND_PREFIX);
        GuildCheckResult guildCheck = discordBot.security.checkGuild(guild);
        if (dbGuild.active != 1) {
            String message = "Thanks for adding me to your guild!\n" +
                    "To see what I can do you can type the command `" + cmdPre + "help`.\n" +
                    "Most of my features are opt-in, which means that you'll have to enable them first. Admins can use `" + cmdPre + "config` to change my settings.\n" +
                    "Most commands has a help portion which can be accessed by typing help after the command; For instance: `" + cmdPre + "skip help`\n\n" +
                    "If you need help or would like to give feedback, feel free to let me know on either `" + cmdPre + "discord` or `" + cmdPre + "github`";
            switch (guildCheck) {
                case TEST_GUILD:
                    message += "\n\n:warning: The guild has been categorized as a test guild. This means that I might leave this guild when the next cleanup happens.\n" +
                            "If this is not a test guild feel free to join my `" + cmdPre + "discord` and ask to have your guild added to the whitelist!";
                    break;
                case BOT_GUILD:
                    message += "\n\n:warning: :robot: Too many bots here, I'm leaving!\n" +
                            "If your guild is not a collection of bots and you actually plan on using me join my `" + cmdPre + "discord` and ask to have your guild added to the whitelist!";
                    break;
                case SMALL:
                case OWNER_TOO_NEW:
                case OKE:
                default:
                    break;
            }
            TextChannel outChannel = null;
            for (TextChannel channel : guild.getTextChannels()) {
                if (channel.canTalk()) {
                    outChannel = channel;
                    break;
                }
            }
            CBotEvent.insert(":house:", ":white_check_mark:",
                    String.format(":id: %s | :hash: %s | :busts_in_silhouette: %s | %s",
                            guild.getId(),
                            dbGuild.id,
                            guild.getMembers().size(),
                            EmojiUtils.shortCodify(guild.getName())).replace("@", "@\u200B"));
            discordBot.getContainer().guildJoined();
            Launcher.log("bot joins guild", "bot", "guild-join",
                    "guild-id", guild.getId(),
                    "guild-name", guild.getName());
            if (outChannel != null) {
                discordBot.out.sendAsyncMessage(outChannel, message, null);
            } else {
                discordBot.out.sendPrivateMessage(owner, message);
            }
            if (guildCheck.equals(GuildCheckResult.BOT_GUILD)) {
                discordBot.queue.add(guild.leave());
            }
            dbGuild.active = 1;
        }
        CGuild.update(dbGuild);
        DiscordBot.LOGGER.info("[event] JOINED SERVER! " + guild.getName());
        for (Member member : event.getGuild().getMembers()) {
            User guildUser = member.getUser();
            int userId = CUser.getCachedId(guildUser.getIdLong(), guildUser.getName());
            OGuildMember guildMember = CGuildMember.findBy(dbGuild.id, userId);
            guildMember.joinDate = new Timestamp(member.getTimeJoined().toInstant().toEpochMilli());
            CGuildMember.insertOrUpdate(guildMember);
        }
    }

    @Override
    public void onGuildLeave(GuildLeaveEvent event) {
        Guild guild = event.getGuild();
        OGuild server = CGuild.findBy(guild.getId());
        server.active = 0;
        CGuild.update(server);
        discordBot.clearGuildData(guild);
        discordBot.getContainer().guildLeft();
        if (server.isBanned()) {
            return;
        }
        Launcher.log("bot leaves guild", "bot", "guild-leave",
                "guild-id", guild.getId(),
                "guild-name", guild.getName());
        CBotEvent.insert(":house_abandoned:", ":fire:",
                String.format(":id: %s | :hash: %s | %s",
                        guild.getId(),
                        server.id,
                        EmojiUtils.shortCodify(guild.getName()).replace("@", "@\u200B")
                ));
    }

    @Override
    public void onMessageReactionAdd(MessageReactionAddEvent event) {
        handleReaction(event, true);
    }

    @Override
    public void onMessageReactionRemove(MessageReactionRemoveEvent event) {
        handleReaction(event, false);
    }

    private void handleReaction(GenericMessageReactionEvent e, boolean adding) {
        if (e.getUser().isBot()) {
            if (!discordBot.security.isInteractionBot(e.getUser().getIdLong())) {
                return;
            }
        }
        if (e.isFromGuild()) {
            TextChannel channel = (TextChannel) e.getChannel();
            if (discordBot.commandReactionHandler.canHandle(channel.getGuild().getIdLong(), e.getMessageIdLong())) {
                discordBot.commandReactionHandler.handle(channel, e.getMessageIdLong(), e.getUser().getIdLong(), e.getReaction());
                return;
            }
            if (!discordBot.gameHandler.executeReaction(e.getUser(), e.getChannel(), e.getReaction(), e.getMessageId())) {
                discordBot.roleReactionHandler.handle(e.getMessageId(), channel, e.getUser(), e.getReactionEmote(), adding);
                return;
            }
        }
        if (e.getChannel().getType().equals(ChannelType.PRIVATE))
            MoveMeCommand.reactionHandler(e.getUser(), e.getReactionEmote(), e.getMessageIdLong(), e.getChannel());
    }

    @Override
    public void onGuildMessageReceived(GuildMessageReceivedEvent event) {
        discordBot.handleMessage(event.getGuild(), event.getChannel(), event.getAuthor(), event.getMessage());
    }

    @Override
    public void onPrivateMessageReceived(PrivateMessageReceivedEvent event) {
        if (event.getAuthor().isBot()) {
            return;
        }
        discordBot.handlePrivateMessage(event.getChannel(), event.getAuthor(), event.getMessage());
    }


    @Override
    public void onGuildBan(GuildBanEvent event) {
        discordBot.logGuildEvent(event.getGuild(), "\uD83D\uDED1", "**" + event.getUser().getName() + "#" + event.getUser().getDiscriminator() + "** has been banned");
    }

    @Override
    public void onGuildMemberUpdateNickname(GuildMemberUpdateNicknameEvent event) {
        String message = "**" + event.getMember().getUser().getName() + "#" + event.getMember().getUser().getDiscriminator() + "** changed nickname ";
        if (event.getOldNickname() != null) {
            message += "from _~~" + event.getOldNickname() + "~~_ ";
        }
        if (event.getNewNickname() != null) {
            message += "to **" + event.getNewNickname() + "**";
        } else {
            message += "back to normal";
        }
        discordBot.logGuildEvent(event.getGuild(), "\uD83C\uDFF7", message);
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        User user = event.getMember().getUser();
        Guild guild = event.getGuild();
        GuildSettings settings = GuildSettings.get(guild);
        OGuildMember guildMember = CGuildMember.findBy(guild.getId(), user.getId());
        boolean firstTime = guildMember.joinDate == null;
        if (firstTime) {
            guildMember.joinDate = new Timestamp(System.currentTimeMillis());
            guildMember.guildId = guild.getId();
            guildMember.userId = user.getId();
            CGuildMember.insertOrUpdate(guildMember);
        }

        if (settings.getBoolValue(GSetting.PM_USER_EVENTS)) {
            discordBot.out.sendPrivateMessage(guild.getOwner().getUser(), String.format("[user-event] **%s#%s** joined the guild **%s**", user.getName(), user.getDiscriminator(), guild.getName()),
                    null
            );
        }
        TextChannel welcomeChannel = discordBot.getWelcomeChannel(guild.getIdLong());
        if (welcomeChannel != null) {
            EmbedBuilder msg = new EmbedBuilder();
            msg.setFooter("Hayasaka Bot by FreshChickenESL", discordBot.getJda().getSelfUser().getAvatarUrl());
            msg.setAuthor(user.getName());
            msg.setColor(new Color(0, 255, 255));
            if (firstTime) {
                msg.setTitle("Welcome to the Boar Hat, " + user.getName());
                msg.setDescription(String.format("Ask a Resident has to use `+verify @%s` to get verified.", ""+user.getIdLong()));
            } else {
                msg.setTitle("Welcome back, " + user.getName());
                msg.setDescription("Usually you should get all your roles back in 1-2 minutes. If that doesnt happen message @FreshChickenESL.");
                DisUtil.recoverUser(event.getMember());
            }
            msg.setImage("https://i.imgur.com/syUYhhE.jpg");
            welcomeChannel.sendMessage(event.getMember().getAsMention()).queue();
            welcomeChannel.sendMessage(msg.build()).queue();
        }

        /*if (settings.getBoolValue(GSetting.PM_USER_EVENTS)) {
            discordBot.out.sendPrivateMessage(guild.getOwner().getUser(), String.format("[user-event] **%s#%s** joined the guild **%s**", user.getName(), user.getDiscriminator(), guild.getName()),
                    null
            );
        }
        discordBot.logGuildEvent(guild, "\uD83D\uDC64", "**" + event.getMember().getUser().getName() + "#" + event.getMember().getUser().getDiscriminator() + "** joined the guild");
        if (settings.getBoolValue(GSetting.WELCOME_NEW_USERS)) {
            TextChannel defaultChannel = discordBot.getDefaultChannel(guild);
            if (defaultChannel != null && defaultChannel.canTalk() && !discordBot.security.isBotAdmin(user.getIdLong())) {
                Template template = firstTime ? Templates.welcome_new_user : Templates.welcome_back_user;
                discordBot.queue.add(defaultChannel.sendMessage(
                        template.formatGuild(guild.getIdLong(), guild, user)),
                        message -> {
                            if (!"no".equals(settings.getOrDefault(GSetting.CLEANUP_MESSAGES))) {
                                discordBot.schedule(() -> discordBot.out.saveDelete(message), Settings.DELETE_MESSAGES_AFTER * 5, TimeUnit.MILLISECONDS);
                            }
                        });
            } else if (defaultChannel != null && defaultChannel.canTalk() && discordBot.security.isBotAdmin(user.getIdLong())) {
                Template template = Templates.welcome_bot_admin;
                discordBot.queue.add(defaultChannel.sendMessage(template.formatGuild(guild.getIdLong(), guild, user)),
                        message -> {
                            if (!"no".equals(settings.getOrDefault(GSetting.CLEANUP_MESSAGES))) {
                                discordBot.schedule(() -> discordBot.out.saveDelete(message), Settings.DELETE_MESSAGES_AFTER * 5, TimeUnit.MILLISECONDS);
                            }
                        });

            }
        }*/
        Launcher.log("user joins guild", "guild", "member-join",
                "guild-id", guild.getId(),
                "guild-name", guild.getName(),
                "user-id", user.getId(),
                "user-name", user.getName());

        if (settings.getBoolValue(GSetting.USER_TIME_RANKS) && !user.isBot()) {
            RoleRankings.assignUserRole(discordBot, guild, user);
        }
    }

    @Override
    public void onGuildMemberLeave(GuildMemberLeaveEvent event) {
        User user = event.getMember().getUser();
        if (user.isBot()) {
            return;
        }
        Guild guild = event.getGuild();
        if (GuildSettings.get(guild).getBoolValue(GSetting.PM_USER_EVENTS)) {
            discordBot.out.sendPrivateMessage(guild.getOwner().getUser(), String.format("[user-event] **%s#%s** left the guild **%s**", user.getName(), user.getDiscriminator(), guild.getName()));
        }
        if (GuildSettings.get(guild).getBoolValue(GSetting.WELCOME_NEW_USERS)) {
            TextChannel defaultChannel = discordBot.getDefaultChannel(guild);
            if (defaultChannel != null && defaultChannel.canTalk()) {
                discordBot.queue.add(defaultChannel.sendMessage(
                        Templates.message_user_leaves.formatGuild(guild.getIdLong(), user, guild)),
                        message -> {
                            if (!"no".equals(GuildSettings.get(guild.getIdLong()).getOrDefault(GSetting.CLEANUP_MESSAGES))) {
                                discordBot.schedule(() -> discordBot.out.saveDelete(message), Settings.DELETE_MESSAGES_AFTER * 5, TimeUnit.MILLISECONDS);
                            }
                        });
            }
        }
        Launcher.log("user leaves guild", "guild", "member-leave",
                "guild-id", guild.getId(),
                "guild-name", guild.getName(),
                "user-id", user.getId(),
                "user-name", user.getName());
        OGuildMember guildMember = CGuildMember.findBy(guild.getIdLong(), user.getIdLong());
        guildMember.joinDate = new Timestamp(System.currentTimeMillis());
        CGuildMember.insertOrUpdate(guildMember);
        discordBot.logGuildEvent(guild, "\uD83C\uDFC3", "**" + user.getName() + "#" + user.getDiscriminator() + "** left the guild");
    }

    @Override
    public void onGuildVoiceJoin(GuildVoiceJoinEvent event) {
        if (event.getMember().getUser().isBot()) {
            return;
        }
        if (event.getMember().getUser().getIdLong() == event.getJDA().getSelfUser().getIdLong()) {
            event.getMember().deafen(true).queue();
        }
    }

    @Override
    public void onGuildVoiceMove(GuildVoiceMoveEvent event) {
        if (!event.getMember().equals(event.getGuild().getSelfMember())) {
            checkLeaving(event.getGuild(), event.getChannelLeft(), event.getMember().getUser());
            onGuildVoiceJoin(new GuildVoiceJoinEvent(event.getJDA(), 0, event.getMember()));
        } else {
            checkLeaving(event.getGuild(), event.getChannelJoined(), event.getMember().getUser());
        }
    }

    @Override
    public void onGuildVoiceLeave(GuildVoiceLeaveEvent event) {
        checkLeaving(event.getChannelLeft().getGuild(), event.getChannelLeft(), event.getMember().getUser());
    }

    private void checkLeaving(Guild guild, VoiceChannel channel, User user) {
        /*if (user.isBot() && !user.equals(user.getJDA().getSelfUser())) {
            return;
        }
        String autoChannel = GuildSettings.get(guild).getOrDefault(GSetting.MUSIC_CHANNEL_AUTO);
        if (!"false".equalsIgnoreCase(autoChannel) && channel.getName().equalsIgnoreCase(autoChannel)) {
            return;
        }
        TextChannel musicChannel = discordBot.getMusicChannel(guild);
        if (musicChannel != null && musicChannel.canTalk()) {
            discordBot.out.sendAsyncMessage(musicChannel, Templates.music.no_one_listens_i_leave.formatGuild(guild.getIdLong()));
        }*/
    }

    @Override
    public void onGuildVoiceGuildDeafen(GuildVoiceGuildDeafenEvent event) {
        Member member = event.getGuild().getMember(event.getJDA().getSelfUser());
        if (member == event.getMember() && !event.isGuildDeafened()) {
            member.deafen(true).queue();
            discordBot.getMusicChannel(event.getGuild()).sendMessage("Please don't undeafen me.").queue();
        }
    }
}