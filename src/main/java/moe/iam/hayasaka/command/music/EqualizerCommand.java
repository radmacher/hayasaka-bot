package moe.iam.hayasaka.command.music;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class EqualizerCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Change equalizer";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "equalizer";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "equalizer highbass                         //enable bass boost",
                "equalizer lowbass                          //enable bass reduction",
                "equalizer start                            //start the equalizer",
                "equalizer stop                             //stop the equalizer",
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "eq"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        Guild guild = inputMessage.getGuild();
        if(args.length > 0){
            if(args[0].equalsIgnoreCase("highbass")){
                bot.audioManager.eqSet(guild, "HIGHBASS", 0.1f);
                channel.sendMessage("Enabling bass boost!").queue();
            }else if(args[0].equalsIgnoreCase("lowbass")){
                bot.audioManager.eqSet(guild, "LOWBASS", -0.1f);
                channel.sendMessage("Disabling bass boost!").queue();
            }else if(args[0].equalsIgnoreCase("start")){
                bot.audioManager.eqStart(guild);
                channel.sendMessage("Enabled equalizer!").queue();
            }else if(args[0].equalsIgnoreCase("stop")){
                bot.audioManager.eqStop(guild);
                channel.sendMessage("Disabled equalizer!").queue();
            }
        }
        bot.audioManager.eqStart(guild);
        return "";
    }
}
