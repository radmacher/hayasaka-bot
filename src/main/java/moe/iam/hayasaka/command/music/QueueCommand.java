package moe.iam.hayasaka.command.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Misc;
import moe.iam.hayasaka.util.TextUtil;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

import java.util.ArrayList;
import java.util.Arrays;

public class QueueCommand extends AbstractCommand {

    private final static String REPEAT = "\uD83D\uDD01"; // 🔁

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Returns the current music queue.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "queue";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[0];
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "q"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        Guild guild = inputMessage.getGuild();
        ArrayList<AudioTrack> songsl = bot.audioManager.getQueue(guild);
        String[] songs = new String[songsl.size()];
        long total = 0;
        for(int i=0; i<songsl.size(); i++)
        {
            total += songsl.get(i).getDuration();
            songs[i] = songsl.get(i).getInfo().title;
        }
        long fintotal = total;
        if(songs.length > 0) {
            return Misc.makeTable(Arrays.asList(songs), 64, 1);
        }else{
            return Templates.music.queue_is_empty.formatGuild(channel, guild);
        }
    }

    private String getQueueTitle(String success, int songslength, long total, boolean repeatmode){
        StringBuilder sb = new StringBuilder();
        return TextUtil.filter(sb.append(success).append(" Current Queue | ").append(songslength)
                .append(" entries | `").append(TextUtil.formatTiming(total, 1000000000000L)).append("` ")
                .append(repeatmode ? "| " + REPEAT : "").toString());
    }
}
