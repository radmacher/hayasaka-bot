package moe.iam.hayasaka.command.music;

import com.google.api.client.repackaged.com.google.common.base.Joiner;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import net.dv8tion.jda.api.entities.*;

public class PlayCommand extends AbstractCommand {

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Play some music";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "play";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "play <yt-link>",
                "play <search term>"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[0];
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length == 0) return "Invalid use!";
        Guild guild = inputMessage.getGuild();
        Member member = guild.getMember(author);
        if (member == null) return Templates.command.cant_find_user.formatGuild(channel);
        VoiceChannel vc = member.getVoiceState().getChannel();
        if (vc == null) return Templates.command.user_not_in_voice_channel.formatGuild(channel);
        if(bot.youtubeSearch.isUrl(args[0])) {
                bot.audioManager.loadAndPlay((TextChannel) channel, args[0], vc);
        }else{
            String url = bot.youtubeSearch.searchYoutube(Joiner.on(" ").join(args));
            bot.audioManager.loadAndPlay((TextChannel) channel, url, vc);
        }
        return "";
    }
}
