package moe.iam.hayasaka.command.music;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;
import com.sedmelluq.discord.lavaplayer.track.AudioTrackInfo;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.util.TextUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

import java.awt.*;
import java.util.concurrent.TimeUnit;

public class NowPlayingCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Returns the currently playing song.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "nowplaying";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[0];
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "current",
                "np"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        Guild guild = inputMessage.getGuild();
        EmbedBuilder eb = new EmbedBuilder();
        if(bot.audioManager.getMusicManager(guild).player.getPlayingTrack() != null){
            AudioTrack track = bot.audioManager.getMusicManager(guild).player.getPlayingTrack();
            AudioTrackInfo info = track.getInfo();
            eb.setColor(Color.magenta.brighter());
            String imgurl = "https://img.youtube.com/vi/" + info.identifier + "/hqdefault.jpg";
            eb.addField("Currently playing:","**" + info.title + "** - " + TextUtil.formatTiming(info.length, 360000L),false );
            eb.setFooter(bot.embeddedFooter);
            eb.setColor(bot.embeddedColor);
            eb.setThumbnail(imgurl);
            channel.sendMessage(eb.build()).queue(m -> {
                m.delete().queueAfter(15, TimeUnit.SECONDS);
            });
        }
        return "";
    }
}
