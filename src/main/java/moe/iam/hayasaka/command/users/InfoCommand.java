package moe.iam.hayasaka.command.users;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.model.OUser;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.DisUtil;
import moe.iam.hayasaka.util.TimeUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class InfoCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Get info about a user";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "info";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[0];
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "lookup"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (!channel.getType().isGuild())
            return Templates.error.command_public_only.formatGuild(channel);
        User user = author;
        if (args.length > 1)
            return Templates.invalid_use.formatGuild(channel);
        if (args.length == 1) {
            user = bot.getJda().getUserById(DisUtil.mentionToId(args[0]));
        }
        if (user == null)
            return Templates.command.cant_find_user.formatGuild(channel);
        OUser record = CUser.findBy(user.getIdLong());
        EmbedBuilder msg = new EmbedBuilder();
        msg.setColor(bot.embeddedColor);
        msg.setTitle("User lookup");
        msg.setAuthor(user.getName(), null, user.getAvatarUrl());
        msg.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        msg.addField("Last seen", TimeUtil.formatTimeStamp(record.last_seen), true);
        msg.addField("Time online", TimeUtil.getRelativeTime(record.time_online.getTime()), true);
        msg.addField("Nickname", record.nickname, true);
        channel.sendMessage(msg.build()).queue();
        return "";
    }
}
