package moe.iam.hayasaka.command.informative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.font.FontUtil;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class TestCommand extends AbstractCommand {

    public TestCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "A test command";
    }

    @Override
    public String getCommand() {
        return "test";
    }

    @Override
    public String[] getUsage() {
        return new String[]{
                "test"
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        return FontUtil.makeCursive("A a - B b - C c - D d - E e - F f - G g - H h - I i - J j - K k - L l - M m - N n - O o - P p - Q q - R r - S s - T t - U u - V v - W w - X x - Y y - Z z");
    }
}
