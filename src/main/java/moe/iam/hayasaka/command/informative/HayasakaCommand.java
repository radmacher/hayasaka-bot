package moe.iam.hayasaka.command.informative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.Launcher;
import moe.iam.hayasaka.settings.Settings;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.GitlabUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import org.json.JSONObject;

public class HayasakaCommand extends AbstractCommand {

    private static final String GITLAB_ID = "" + 20228371;

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Get some information about me";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "hayasaka";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "hayasaka pipeline (successful)     //get some details about the latest pipeline",
                "hayasaka version                   //get some information about my version",
                "hayasaka update                    //update me to the latest stable release",
                "hayasaka invite                    //generates a link that lets you add me to your guild",
                "hayasaka commit                    //get details about the latest commit"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[0];
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        EmbedBuilder msg = new EmbedBuilder();
        msg.setColor(bot.embeddedColor);
        msg.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        if (args.length < 1) return Templates.invalid_use.formatGuild(channel);
        switch (args[0]) {
            case "pipeline":
                boolean filterSuccessful = args.length == 2 && args[1].equalsIgnoreCase("successful");
                JSONObject pipelineData = GitlabUtil.getLatestPipeline(filterSuccessful);
                if (pipelineData == null) return "I could not fetch any Pipeline data.";
                JSONObject detailedStatus = pipelineData.getJSONObject("detailed_status");
                if (detailedStatus == null) return "I could not the detailed status of that Pipeline.";
                JSONObject user = pipelineData.getJSONObject("user");
                if (user == null) return "I could not find any information about the user of that Pipeline";
                msg.setTitle(filterSuccessful ? "Latest successful Pipeline results" :  "Latest Pipeline results");
                msg.setAuthor("Click to open in browser", pipelineData.getString("web_url"));
                msg.setThumbnail(detailedStatus.getString("favicon"));
                msg.addField("Status", pipelineData.getString("status"), true);
                msg.addField("Started by", user.getString("name"), true);
                msg.addField("Branch", pipelineData.getString("ref"), true);
                channel.sendMessage(msg.build()).queue();
                return "";
            case "invite":
                msg.setTitle("Click here to invite me", "https://discord.com/oauth2/authorize?client_id=" + Settings.BOT_CLIENT_ID + "&scope=bot&permissions=8");
                msg.setImage("https://i.imgur.com/9rmWp3X.gif");
                channel.sendMessage(msg.build()).queue();
                return "";
            case "version":
                msg.addField("Running version", Launcher.getVersion().toString(), false);
                msg.addField("Latest Version", "", false);
                channel.sendMessage(msg.build()).queue();
                return "";
            case "commit":
                JSONObject commitData = GitlabUtil.getLatestCommit();
                if (commitData == null) return "I could not fetch the latest commit.";
                JSONObject stats = commitData.getJSONObject("stats");
                if (stats == null) return "Could not fetch any stets from the latest commit.";
                msg.setTitle("Latest commit", commitData.getString("web_url"));
                msg.setDescription(commitData.getString("message"));
                msg.addField("Additions", ""+stats.get("additions"), true);
                msg.addField("Deletions", ""+stats.get("deletions"), true);
                msg.addField("Pipeline Status", commitData.getString("status"), false);
                channel.sendMessage(msg.build()).queue();
                return "";
            case "update":
                return "Not yet implemented";
            default:
                return Templates.invalid_use.formatGuild(channel);
        }
    }
}
