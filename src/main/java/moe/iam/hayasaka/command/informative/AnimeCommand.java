package moe.iam.hayasaka.command.informative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.MediaUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import org.json.JSONObject;

import java.awt.*;

public class AnimeCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Lookup information on Anime";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "anime";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "anime <title>"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[0];
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing 38691
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length >= 1) {
            EmbedBuilder builder = new EmbedBuilder();
            JSONObject anime = MediaUtil.getAnime(args);
            if (anime == null)
                return "No anime found.";
            builder.setTitle(anime.getString("title"));
            builder.setAuthor("MyAnimeList", anime.getString("url"));
            builder.addField("Status", isFieldAvailable(anime, "status") ? ""+anime.get("status") : "N/A", false);
            builder.addField("Episodes", isFieldAvailable(anime, "episodes") ? ""+anime.get("episodes") : "N/A", false);
                builder.addField("Score", isFieldAvailable(anime, "score") ? anime.getDouble("score")+"" : "N/A",false);
            builder.addField("Rank", isFieldAvailable(anime, "rank") ? anime.get("rank")+"" : "N/A", false);
            builder.setThumbnail(anime.getString("image_url"));
            builder.setColor(new Color(46, 81, 162,1));
            channel.sendMessage(builder.build()).queue();
            return "";
        }
        return Templates.invalid_use.formatGuild(channel);
    }

    public static boolean isFieldAvailable(JSONObject json, String field) {
        return json.has(field) && json.get(field) != null && !json.get(field).toString().equals("null");
    }
}
