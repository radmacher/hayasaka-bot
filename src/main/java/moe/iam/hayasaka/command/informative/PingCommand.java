package moe.iam.hayasaka.command.informative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

/**
 * !ping
 */
public class PingCommand extends AbstractCommand {
    private static final String[] pingMessages = new String[]{
            ":ping_pong::white_small_square::black_small_square::black_small_square::ping_pong:",
            ":ping_pong::black_small_square::white_small_square::black_small_square::ping_pong:",
            ":ping_pong::black_small_square::black_small_square::white_small_square::ping_pong:",
            ":ping_pong::black_small_square::white_small_square::black_small_square::ping_pong:",
    };

    public PingCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "checks the latency of the bot";
    }

    @Override
    public String getCommand() {
        return "ping";
    }

    @Override
    public String[] getUsage() {
        return new String[]{
                "ping                         //Check bot latency",
                "ping fancy                   //Check bot latency in a fancier way"
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {

        if (args.length > 0 && args[0].matches("fancy")) {
            bot.queue.add(channel.sendMessage("Checking ping..."), message -> {
                int pings = 5;
                int lastResult;
                int sum = 0, min = 999, max = 0;
                long start = System.currentTimeMillis();
                for (int j = 0; j < pings; j++) {
                    message.editMessage(pingMessages[j % pingMessages.length]).complete();
                    lastResult = (int) (System.currentTimeMillis() - start);
                    sum += lastResult;
                    min = Math.min(min, lastResult);
                    max = Math.max(max, lastResult);
                    try {
                        Thread.sleep(1_500L);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    start = System.currentTimeMillis();
                }
                message.editMessage(String.format("Average ping is %dms (min: %d, max: %d)", (int) Math.ceil(sum / 5f), min, max)).complete();
            });
        } else {
            long start = System.currentTimeMillis();
            bot.queue.add(channel.sendMessage(":outbox_tray: checking ping"),
                    message -> bot.queue.add(message.editMessage(":inbox_tray: ping is " + (System.currentTimeMillis() - start) + "ms")));
            return "";
        }
        return "";
    }
}