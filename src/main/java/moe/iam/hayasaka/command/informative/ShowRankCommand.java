package moe.iam.hayasaka.command.informative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.DisUtil;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class ShowRankCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Show you which bot-rank you currently have.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "showrank";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "showrank [@user]"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[]{
                "srank",
                "botrank"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length > 1)
            return Templates.invalid_use.formatGuild(channel);
        User user = null;
        if (args.length == 0)
            user = author;
        if (args.length == 1 ) {
            if (DisUtil.isUserMention(args[0])) {
                if (DisUtil.mentionToId(args[0]) != null) {
                    user = bot.getJda().getUserById(DisUtil.mentionToId(args[0]));
                }
            } else if (DisUtil.extractId(args[0]) != null) {
                user = bot.getJda().getUserById(DisUtil.extractId(args[0]));
            }
            if (user != null) {
                if (!inputMessage.getGuild().isMember(user)) {
                    return "That user is not a member of this guild.";
                }
            }
        }
        return "Your rank is " + bot.security.getSimpleRankForGuild(author, inputMessage.getGuild()).name();
    }
}
