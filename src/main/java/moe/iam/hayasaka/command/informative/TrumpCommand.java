package moe.iam.hayasaka.command.informative;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import org.json.JSONArray;
import org.json.JSONObject;

public class TrumpCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Get a random donald trump quote.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "trump";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[]{};
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        try {
            HttpResponse<JsonNode> response = Unirest.get("https://www.tronalddump.io/random/quote")
                    .header("accept", "application/json")
                    .asJson();
            JSONObject baseObj = (JSONObject) response.getBody().getObject();
            JSONObject embedded = baseObj.getJSONObject("_embedded");
            JSONArray source = embedded.getJSONArray("source");
            JSONObject sourceObj = source.getJSONObject(0);

            return  sourceObj.getString("url");
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return "Could not fetch tweet.";
    }
}
