package moe.iam.hayasaka.command.bot_administration;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.templates.Templates;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

import java.io.IOException;

/**
 * avatar
 * manage avatar
 */
public class ChangeAvatarCommand extends AbstractCommand {
    public ChangeAvatarCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "Changes my avatar";
    }

    @Override
    public String getCommand() {
        return "updateavatar";
    }

    @Override
    public boolean isListed() {
        return false;
    }

    @Override
    public String[] getUsage() {
        return new String[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[]{
                "avatar"
        };
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        SimpleRank rank = bot.security.getSimpleRank(author);

        if (!rank.isAtLeast(SimpleRank.CREATOR)) {
            return Templates.no_permission.formatGuild(channel);
        }
        if (args.length <= 1) {
            try {
                Icon icon = Icon.from(Unirest.get(args[0]).asBinary().getBody());
                bot.queue.add(channel.getJDA().getSelfUser().getManager().setAvatar(icon));
            } catch (IOException | UnirestException e) {
                return "Error: " + e.getMessage();
            }
            return ":+1:";
        }
        return ":face_palm: I expected you to know how to use it";
    }
}