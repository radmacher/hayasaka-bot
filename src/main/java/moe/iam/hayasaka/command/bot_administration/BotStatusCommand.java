package moe.iam.hayasaka.command.bot_administration;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.Misc;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

/**
 * botstatus
 * changes the bot status (the playing game, streaming, etc)
 */
public class BotStatusCommand extends AbstractCommand {
    public BotStatusCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "Set the game I'm currently playing";
    }

    @Override
    public String getCommand() {
        return "botstatus";
    }

    @Override
    public String[] getUsage() {
        return new String[]{
                "botstatus reset                      //unlocks the status",
                "botstatus playing <game>                //changes the playing game to <game>",
                "botstatus streaming <username> <game>   //streaming twitch.tv/<username> playing <game>",
                "botstatus watching <movie>              //changes the status to watching <movie>",
                "botstatus listening <song>              //changes the status to listening to <song>"
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{
                "setstatus"
        };
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        SimpleRank rank = bot.security.getSimpleRank(author);
        if (!rank.isAtLeast(SimpleRank.BOT_ADMIN)) {
            return Templates.no_permission.formatGuild(channel);
        }
        if (args.length == 0) {
            return Templates.invalid_use.formatGuild(channel);
        }
        if (args.length > 0) {
            switch (args[0].toLowerCase()) {
                case "reset":
                    bot.getContainer().setStatusLocked(false);
                    return Emojibet.THUMBS_UP;
                case "game":
                case "playing":
                    if (args.length < 2) {
                        return Templates.invalid_use.formatGuild(channel);
                    }
                    channel.getJDA().getPresence().setActivity(Activity.playing(Misc.joinStrings(args, 1)));
                    break;
                case "stream":
                case "streaming":
                    if (args.length < 3) {
                        return Templates.invalid_use.formatGuild(channel);
                    }
                    try {
                        channel.getJDA().getPresence().setActivity(Activity.streaming(Misc.joinStrings(args, 2), "http://www.twitch.tv/" + args[1]));
                    } catch (Exception e) {
                        return Emojibet.THUMBS_DOWN + " " + e.getMessage();
                    }
                    break;
                case "listening":
                    if (args.length < 2) {
                        return Templates.invalid_use.formatGuild(channel);
                    }
                    channel.getJDA().getPresence().setActivity(Activity.listening(Misc.joinStrings(args, 1)));
                    break;
                case "watching":
                    if (args.length < 2) {
                        return Templates.invalid_use.formatGuild(channel);
                    }
                    break;
                default:
                    return Templates.invalid_use.formatGuild(channel);
            }
            bot.getContainer().setStatusLocked(true);
            try {
                Thread.sleep(5_000L);
            } catch (InterruptedException ignored) {
            }
            return Emojibet.THUMBS_UP;
        }
        return Templates.invalid_use.formatGuild(channel);
    }
}