package moe.iam.hayasaka.command.bot_administration;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.model.OUser;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Misc;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

/**
 * ban a user from a guild
 */
public class GlobalBanCommand extends AbstractCommand {
    public GlobalBanCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "Ban those nasty humans";
    }

    @Override
    public String getCommand() {
        return "globalban";
    }

    @Override
    public String[] getUsage() {
        return new String[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        SimpleRank rank = bot.security.getSimpleRank(author);
        if (rank.isAtLeast(SimpleRank.BOT_ADMIN) && args.length >= 1) {
            boolean unban = args.length > 1 && Misc.isFuzzyFalse(args[1]);
            OUser user = CUser.findBy(Long.parseLong(args[0]));
            user.banned = unban ? 0 : 1;
            if (user.id == 0) {
                return "User `" + args[0] + "` not found";
            }
            CUser.update(user);
            if (unban) {
                bot.security.removeUserBan(Long.parseLong(user.discord_id));
                return "`" + user.name + "` (`" + user.discord_id + "`) has been globally unbanned";
            } else {
                bot.security.addUserBan(Long.parseLong(user.discord_id));
                return "`" + user.name + "` (`" + user.discord_id + "`) has been globally banned";
            }
        }
        return Templates.no_permission.formatGuild(channel);
    }
}