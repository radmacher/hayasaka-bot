package moe.iam.hayasaka.command.media;

import com.google.common.base.Joiner;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.command.meta.CommandReactionListener;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.*;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class JellyfinCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Lookup stuff on the media server.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "jellyfin";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {

        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "media"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        TextChannel txt = (TextChannel) channel;
        Guild guild = txt.getGuild();
        SimpleRank rank = bot.security.getSimpleRank(author);
        if (!rank.isAtLeast(SimpleRank.MEDIA))
            return Templates.no_permission.formatGuild(channel);
        if (args.length < 2)
            return Templates.invalid_use.formatGuild(channel);
        switch (args[0]) {
            case "search":
                //JSONArray media = MediaUtil.searchJellyfin(MediaUtil.getUrlString(Arrays.copyOfRange(args, 1, args.length)));
                List<MediaObject> results = MediaUtil.parseJellyfinSearch(MediaUtil.searchJellyfin(MediaUtil.getUrlString(Arrays.copyOfRange(args, 1, args.length))));
                String ret = "Results for: " + Joiner.on(" ").join(Arrays.copyOfRange(args, 1, args.length)) + "\n\n";
                int i = 0;
                final ArrayList<Pair<String, MediaObject>> reactions = new ArrayList<>();
                for (MediaObject result : results) {
                    ++i;
                    ret += String.format("%s %s\n", Misc.numberToEmote(i), result.title);
                    reactions.add(new Pair<>(Misc.numberToEmote(i), result));
                }
                ret += "\nYou can pick a result by clicking one of the reactions";
                txt.sendMessage(ret).queue(msg -> {
                    CommandReactionListener<Integer> listener = new CommandReactionListener<>(author.getIdLong(), null);
                    for (Pair<String, MediaObject> reaction : reactions) {
                        listener.registerReaction(reaction.getKey(),
                                message -> {
                                    listener.disable();
                                    message.delete().queue();
                                    searchResult(author, txt, bot, reaction.getValue());
                                });
                    }
                    bot.commandReactionHandler.addReactionListener(guild.getIdLong(), msg, listener);
                });
                break;
            case "lookup":
            case "info":
            case "details":
                String x;
                break;
            default:
                return Templates.invalid_use.formatGuild(channel);
        }
        return "";
    }

    private static void searchResult(User user, TextChannel channel, DiscordBot bot, MediaObject mediaObject) {
        Guild guild = channel.getGuild();
        EmbedBuilder embeddedMessage = new EmbedBuilder();
        embeddedMessage.setTitle(mediaObject.title);
        embeddedMessage.setColor(bot.embeddedColor);
        embeddedMessage.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        embeddedMessage.setAuthor("Open in browser", MediaUtil.jellyfinUrl(mediaObject.itemId));

        channel.sendMessage(embeddedMessage.build()).queue(msg -> {
            CommandReactionListener<Integer> listener = new CommandReactionListener<>(user.getIdLong(), null);
            listener.registerReaction(Emojibet.X,
                    message -> {
                        listener.disable();
                        message.delete().queue();
                    });
            listener.registerReaction(Emojibet.INFORMATION,
                    message -> {
                        listener.disable();
                        message.delete().queue();
                        moreDetails(user, channel, bot, mediaObject);
                    });
            bot.commandReactionHandler.addReactionListener(guild.getIdLong(), msg, listener);
        });
    }

    private static void moreDetails(User user, TextChannel channel, DiscordBot bot, MediaObject mediaObject) {
        Guild guild = channel.getGuild();
        EmbedBuilder embeddedMessage = new EmbedBuilder();
        embeddedMessage.setColor(bot.embeddedColor);
        embeddedMessage.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        embeddedMessage.setImage(MediaUtil.getPrimaryImage(mediaObject.itemId));
        embeddedMessage.setTitle(mediaObject.title);
        embeddedMessage.setAuthor("Open in browser", MediaUtil.jellyfinUrl(mediaObject.itemId));
        JSONObject details = MediaUtil.getAnime(mediaObject.title.replaceAll(" ", "%20"));
        if (details != null) {
            embeddedMessage.setDescription(details.getString("synopsis"));
            embeddedMessage.addField("Rating", details.get("score")+"", true);
            embeddedMessage.addField("Popularity", details.get("popularity")+"", true);
            JSONArray seasons = MediaUtil.getSeasons(mediaObject.itemId);
            if (seasons != null) embeddedMessage.addField("Seasons on Server", seasons.length()+"", true);
        }
        channel.sendMessage(embeddedMessage.build()).queue(msg -> {
            CommandReactionListener<Integer> listener = new CommandReactionListener<>(user.getIdLong(), null);
            listener.registerReaction(Emojibet.X,
                    message -> {
                        listener.disable();
                        message.delete().queue();
                    });
            bot.commandReactionHandler.addReactionListener(guild.getIdLong(), msg, listener);
        });
    }
}
