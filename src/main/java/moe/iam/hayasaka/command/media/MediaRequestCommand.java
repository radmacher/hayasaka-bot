package moe.iam.hayasaka.command.media;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.command.meta.CommandReactionListener;
import moe.iam.hayasaka.db.controllers.CJellyfin;
import moe.iam.hayasaka.db.model.OJellyfin;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.MediaUtil;
import moe.iam.hayasaka.util.Misc;
import moe.iam.hayasaka.util.TimeUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MediaRequestCommand extends AbstractCommand {

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Request something for the media server.";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "mediarequest";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "mediarequest <anime/movie/series> <title>"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "mreq",
                "mediareq",
                "mrequest"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length < 1) return Templates.invalid_use.formatGuild(channel);
        if (!channel.getType().isGuild()) {
            return Templates.command.not_for_private.formatGuild(channel);
        }

        Action action = Action.getFromString(args[0]);
        if (action != null) {
            SimpleRank rank = bot.security.getSimpleRank(author);
            if (!rank.isAtLeast(action.requiredRank)) return Templates.no_permission.formatGuild(channel);
            if (action == Action.LIST) {
                List<OJellyfin> records = CJellyfin.getAllRequests();
                List<List<String>> body = new ArrayList<>();
                records.forEach(record -> body.add(Arrays.asList(record.title, record.url, bot.getJda().getUserById(record.user_id).getName(), TimeUtil.formatTimeStamp(record.date))));
                return Misc.makeAsciiTable(Arrays.asList("Title", "URL", "Requester", "Date"), body, null);
            }
            if (args.length < 3) return Templates.invalid_use.formatGuild(channel);
            Medium medium = Medium.getFromString(args[1]);
            if (medium == null) return Templates.invalid_use.formatGuild(channel);
            handleMessage(bot, channel, author, inputMessage.getGuild(), action, medium, MediaUtil.getUrlString(Arrays.copyOfRange(args, 2, args.length)));
            return "";
        }

        Medium medium = Medium.getFromString(args[0]);
        if (medium == null) return Templates.invalid_use.formatGuild(channel);
        action = Action.REQUEST;
        if (args.length < 2) return Templates.invalid_use.formatGuild(channel);
        handleMessage(bot, channel, author, inputMessage.getGuild(), action, medium, MediaUtil.getUrlString(Arrays.copyOfRange(args, 1, args.length)));
        return "";
    }

    private enum Medium {
        ANIME,
        MOVIE,
        SERIES;

        private static Medium getFromString(String string) {
            for (Medium medium : Medium.values()) {
                if (medium.name().equalsIgnoreCase(string)) return medium;
            }
            return null;
        }
    }

    private enum Action {
        REQUEST("request", "requested", SimpleRank.MEDIA),
        BAN("ban", "banned", SimpleRank.BOT_ADMIN),
        UNBAN("unban", "unbanned", SimpleRank.BOT_ADMIN),
        LIST("", "", SimpleRank.BOT_ADMIN);

        private String present;
        private String past;
        private SimpleRank requiredRank;

        Action(String present, String past, SimpleRank requiredRank) {
            this.present = present;
            this.past = past;
            this.requiredRank = requiredRank;
        }

        private static Action getFromString(String string) {
            for (Action action : Action.values()) {
                if (action.name().equalsIgnoreCase(string)) return action;
            }
            return null;
        }
    }

    private static void handleMessage(DiscordBot bot, MessageChannel channel, User author, Guild guild, Action action, Medium medium, String title) {
        TextChannel txt = (TextChannel) channel;
        EmbedBuilder embeddedMessage = new EmbedBuilder();
        embeddedMessage.setColor(bot.embeddedColor);
        embeddedMessage.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        SimpleRank rank = bot.security.getSimpleRank(author);
        if (!rank.isAtLeast(action.requiredRank)) {
            channel.sendMessage(Templates.no_permission.formatGuild(channel)).queue();
            return;
        }
        switch (medium) {
            case ANIME:
                JSONObject anime = MediaUtil.getAnime(title);
                if (anime == null) {
                    channel.sendMessage("Could not find anime.").queue();
                    return;
                }
                embeddedMessage.setAuthor("Click to open MAL", anime.getString("url"));
                embeddedMessage.addField("Title", anime.getString("title"), true);
                embeddedMessage.addField("Episodes", anime.getInt("episodes")+"", true);
                embeddedMessage.addField("Status", anime.getString("status"), true);
                embeddedMessage.setThumbnail(anime.getString("image_url"));
                embeddedMessage.setTitle("Do you want to " + action.present + " " + anime.getString("title") + "?");

                channel.sendMessage(embeddedMessage.build()).queue(msg -> {
                    CommandReactionListener<Integer> listener = new CommandReactionListener<>(author.getIdLong(), null);
                    listener.registerReaction(Emojibet.CHECK_MARK_GREEN,
                            message -> {
                                listener.disable();
                                message.delete().queue();
                                manageReaction(anime.getString("title"), anime.getString("url"), author, txt, action, medium, bot);
                            });
                    listener.registerReaction(Emojibet.TRASH_CAN,
                            message -> {
                                listener.disable();
                                message.delete().queue();
                            });
                    bot.commandReactionHandler.addReactionListener(guild.getIdLong(), msg, listener);
                });

                break;
            case MOVIE:
            case SERIES:
                embeddedMessage.setDescription("NOT YET IMPLEMENTED!");
                break;
            default:
                break;
        }
    }

    private static void manageReaction(String title, String url, User user, TextChannel channel, Action action, Medium medium, DiscordBot bot) {
        OJellyfin record = CJellyfin.findBy(title);
        if (record.id == 0) {
            record.title = title;
            record.url = url;
            record.user_id = user.getId();
            record.banned = action == Action.BAN ? 1 : 0;
            record.date = new Date(System.currentTimeMillis());
            record.medium = medium.name();
        } else if (record.banned == 1) {
            channel.sendMessage(title + " is banned from being requested.").submit()
                    .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                    .whenComplete((s, error) -> {
                        if (error != null) error.printStackTrace();
                    });
            return;
        } else if (action == Action.REQUEST) {
            User requester = bot.getJda().getUserById(record.user_id);
            String msg;
            if (requester != null) {
                msg = "That " + medium.name().toLowerCase() + " was already requested by " + requester.getName();
                channel.sendMessage(msg).submit()
                        .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                        .whenComplete((s, error) -> {
                            if (error != null) error.printStackTrace();
                        });
                return;
            }
        } else {
            switch (action) {
                case BAN:
                    record.banned = 1;
                    break;
                case UNBAN:
                    record.banned = 0;
                    break;
            }
        }
        channel.sendMessage("Successfully " + action.past + " " + title + ".").submit()
                .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                .whenComplete((s, error) -> {
                    if (error != null) error.printStackTrace();
                });
        CJellyfin.update(record);
    }

    /*private static void manageReaction(String title, String url, User addedBy, TextChannel channel, Action action, Medium medium) {
        OJellyfin record = CJellyfin.findBy(title);
        if (record.id == 0) {
            record.url = url;
            record.title = title;
            record.date = new Date(System.currentTimeMillis());
            switch (action) {
                case BAN:
                    record.banned = 1;
                    break;
                case UNBAN:
                case REQUEST:
                    record.banned = 0;
                    break;
            }
            record.banned = action == Action.BAN ? 1 : 0;
            record.user_id = addedBy.getId();
            channel.sendMessage("Successfully " + action.past + " " + title + ".").submit()
                    .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                    .whenComplete((s, error) -> {
                        if (error != null) error.printStackTrace();
                    });
            CJellyfin.update(record);
        } else {
            if (record.banned == 1) {
                channel.sendMessage(title + " is banned from being requested.").submit()
                        .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                        .whenComplete((s, error) -> {
                            if (error != null) error.printStackTrace();
                        });
                return;
            }
            channel.sendMessage("That anime was already requested by " + addedBy.getName()).submit()
                    .thenCompose((m) -> m.delete().submitAfter(3, TimeUnit.SECONDS))
                    .whenComplete((s, error) -> {
                        if (error != null) error.printStackTrace();
                    });
        }
    }*/
}
