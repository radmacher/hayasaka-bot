package moe.iam.hayasaka.command.moderation;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.handler.GuildSettings;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.DisUtil;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.TimeUtil;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;

public class VerifyCommand extends AbstractCommand {

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Grants a user access to the server";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "verify";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                        "verify <@user>",
                        "verify <id>"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[0];
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        Guild guild = inputMessage.getGuild();
        Role role = GuildSettings.get(guild).getRoleValue(GSetting.NEW_USER_ROLE, guild);
        User user = null;
        Member member = guild.getMember(user);
        if (args.length != 1)
            return Templates.invalid_use.formatGuild(channel);
        String userId = DisUtil.extractId(args[0]);
        if (userId == null)
            return Templates.command.cant_find_user.formatGuild(channel);
        user = bot.getJda().getUserById(userId);
        //role not defined
        if (role == null)
            return Templates.command.verify_role_not_defined.formatGuild(channel);
        //user not defined or not found
        if (user == null)
            return Templates.command.pm_cant_find_user.formatGuild(channel);
        //user not member of guild
        if (member == null)
            return Templates.command.verify_user_not_memeber.formatGuild(channel);
        //user is already verified
        if (!member.getRoles().isEmpty())
            return Templates.command.user_already_verified.formatGuild(channel);
        guild.addRoleToMember(user.getId(), role).queue();
        inputMessage.addReaction(Emojibet.CHECK_MARK_GREEN).queue();

        //sent modlog message
        EmbedBuilder msg = new EmbedBuilder();
        msg.setTitle(user.getAsTag() + " has been verified");
        msg.setAuthor(author.getAsTag(), null, author.getAvatarUrl());
        msg.setFooter("Message ID: " + inputMessage.getId() + " | ⌚ " + TimeUtil.getCurrentTimeString());
        msg.addField("Verifier ID:", author.getId(), false);
        msg.addField("Verified ID:", user.getId(), false);
        bot.getModlogChannel(guild.getIdLong()).sendMessage(msg.build()).queue();

        return "";
    }
}
