package moe.iam.hayasaka.command.songoda;

import com.google.api.client.repackaged.com.google.common.base.Joiner;
import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.command.meta.CommandReactionListener;
import moe.iam.hayasaka.core.Logger;
import moe.iam.hayasaka.handler.CommandHandler;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.Misc;
import moe.iam.hayasaka.util.Pair;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import sll.coding.songodaapi.Resource;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class SongodaCommand extends AbstractCommand {

    //TODO: implement discord error logging.

    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Some useful tools for the Songoda Marketplace";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "songoda";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[]{
                "songoda search <plugin>            //search for a plugin on the marketplace",
                "songoda slug <slug>              //use a plugins slug to search for it"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[]{};
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length == 0)
            return Templates.invalid_use.formatGuild(channel);
        switch (args[0]) {
            case "info":
            case "search":
            case "lookup":
                return lookup(bot, args, (TextChannel) channel, author);
            case "slug":
                fromSlug(bot, args[1], (TextChannel) channel);
                return "";
            default:
                return Templates.invalid_use.formatGuild(channel);
        }
    }

    private static String lookup(DiscordBot bot, String[] args, TextChannel channel, User author) {
        if (args.length < 2)
            return Templates.invalid_use.formatGuild(channel);
        String searchCriteria = Joiner.on(" ").join(Arrays.copyOfRange(args, 1, args.length));
        String ret = "Results for: " + searchCriteria + "\n\n";
        int i = 0;
        final ArrayList<Pair<String, String>> reactions = new ArrayList<>();

        ArrayList<Resource> results;
        try {
            results = new ArrayList<Resource>(Resource.fromName(searchCriteria, 10).getResults());
        } catch (IOException e) {
            Logger.fatal(e);
            return "An error occurred. Please try again!";
        }
        if (results.size() == 0) {
            return "No results found for that search term.";
        }
        if (results.size() == 1) {
            AbstractCommand songodaPlugin = CommandHandler.getCommand("songoda");
            if (songodaPlugin != null) {
                songodaPlugin.execute(bot, new String[]{"slug", results.get(0).getSlug()}, channel, author, null);
            }
            return "";
        }

        for (Resource result : results) {
            ++i;
            ret += String.format("%s %s\n", Misc.numberToEmote(i), result.getName());
            reactions.add(new Pair<>(Misc.numberToEmote(i), result.getSlug()));
        }
        channel.sendMessage(ret).queue(msg -> {
            CommandReactionListener<Integer> listener = new CommandReactionListener<>(author.getIdLong(), null);
            for (Pair<String, String> reaction : reactions) {
                listener.registerReaction(reaction.getKey(),
                        message -> {
                            listener.disable();
                            message.delete().queue();
                            AbstractCommand songodaPlugin = CommandHandler.getCommand("songoda");
                            if (songodaPlugin != null) {
                                songodaPlugin.execute(bot, new String[]{"slug", reaction.getValue()}, channel, author, null);
                            }
                        });
            }
            bot.commandReactionHandler.addReactionListener(channel.getGuild().getIdLong(), msg, listener);
        });
        return "";
    }

    private static void fromSlug(DiscordBot bot, String slug, TextChannel channel) {
        Resource resource;
        try {
            resource = Resource.fromSlug(slug);
        } catch (IOException e) {
            Logger.fatal(e);
            channel.sendMessage("An error occurred. Please try again!").queue();
            return;
        }
        EmbedBuilder msg = new EmbedBuilder();
        msg.setColor(new Color(0xFC494A));
        msg.setFooter(bot.embeddedFooter, bot.getJda().getSelfUser().getAvatarUrl());
        msg.setAuthor("Songoda Marketplace lookup", "https://songoda.com/marketplace", "https://cdn2.songoda.com/branding/icon.png");
        msg.setTitle(resource.getName(), resource.getUrl());
        msg.setDescription(resource.getDescription());
        msg.addField("Version", resource.getVersions().get(0).getVersion(), true);
        msg.addField("Price", resource.getPrice()+resource.getCurrency(), true);
        msg.addField("Rating", resource.getRating(), true);
        channel.sendMessage(msg.build()).queue();
    }
}
