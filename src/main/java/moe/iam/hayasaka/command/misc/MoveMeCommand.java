package moe.iam.hayasaka.command.misc;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import moe.iam.hayasaka.util.DisUtil;
import moe.iam.hayasaka.util.Emojibet;
import moe.iam.hayasaka.util.TextUtil;
import net.dv8tion.jda.api.entities.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class MoveMeCommand extends AbstractCommand {
    public static List<MoveMeObject> users = new ArrayList<>();

    //FIXME: this is a mess


    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Request to join a users voice channel";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "moveme";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[] {
                "moveme <@user>         //move to users channel",
                "moveme <channel>       //move to a specified channel"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[] {
                "mm"
        };
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length != 1)
            return Templates.invalid_use.formatGuild(channel);

        User user = bot.getJda().getUserById(DisUtil.mentionToId(args[0]));
        if (user == null)
            return Templates.command.cant_find_user.formatGuild(channel);
        Member member = inputMessage.getGuild().getMember(user);
        if (member == null)
            return Templates.command.cant_find_user.formatGuild(channel);
        if (member.getVoiceState() == null)
            return Templates.command.cant_find_user.formatGuild(channel);
        if (!member.getVoiceState().inVoiceChannel())
            return Templates.command.user_not_in_voice_channel.formatGuild(channel);
        if (member.getVoiceState().getChannel() == null)
            return Templates.command.user_not_in_voice_channel.formatGuild(channel);

        for (MoveMeObject object : users) {
            if (object.getRequester().equals(author)) {
                if ((object.getTimestamp().getTime()+900000) < System.currentTimeMillis())
                    return "You can only request to join a user every 15 minutes.";
                users.remove(object);
                break;
            }

        }
        try {
            user.openPrivateChannel().submit()
                    .thenCompose(dm -> dm.sendMessage(author.getAsTag() + " wants to join your channel. Do you want me to move him?").submit()
                            .whenComplete((message, error) -> {
                                if (error != null) TextUtil.sendSelfDestroyingMessage(bot, channel, "That user has disabled DMs or blocked me. I won't be able to ask him for his permission.", 3, TimeUnit.SECONDS);
                                else {
                                    users.add(new MoveMeObject(author, user, new Date(System.currentTimeMillis()), member.getVoiceState().getChannel(), message, inputMessage.getGuild()));
                                    TextUtil.sendSelfDestroyingMessage(bot, channel, "Your move request has been sent. Please wait in a voice channel.", 3, TimeUnit.SECONDS);
                                }
                            })).get().addReaction(Emojibet.CHECK_MARK_GREEN).queue();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }


        return "";
    }

    public static void reactionHandler(User user, MessageReaction.ReactionEmote emote, long messageId, MessageChannel channel) {
        MoveMeObject removeMe = null;
        for (MoveMeObject obj : users) {
            if (obj.getMessage().getIdLong() == messageId) {
                if ((obj.getTimestamp().getTime()-900000) > System.currentTimeMillis()) {
                    channel.sendMessage("That request expired.").queue();
                    return;
                }
                moveMember(obj.getAddressee(), obj.getRequester(), obj.getGuild(), channel);
                removeMe = obj;
            }
        }
        users.remove(removeMe);
    }

    public static void moveMember(User moveMe, User destination, Guild guild, MessageChannel channel) {
        Member targetMember = guild.getMember(moveMe);
        if (targetMember == null) {
            channel.sendMessage("I can not find you on any of my servers.").queue();
            return;
        }
        if (targetMember.getVoiceState() == null) {
            channel.sendMessage("I could not get your voice state.").queue();
            return;
        }
        if (!targetMember.getVoiceState().inVoiceChannel()) {
            channel.sendMessage("You don't seem to be in a voice channel.").queue();
            return;
        }
        if (targetMember.getVoiceState().getChannel() == null) {
            channel.sendMessage("I could not find your channel. Is it hidden?").queue();
            return;
        }
        Member moveMeMember = guild.getMember(destination);
        if (moveMeMember == null) {
            channel.sendMessage("I can suddenly no longer find the user that tried to join you. Is he still on my server?").queue();
            return;
        }
        if (moveMeMember.getVoiceState() == null) {
            channel.sendMessage("I could not get the users voice state.").queue();
            return;
        }
        if (!moveMeMember.getVoiceState().inVoiceChannel()) {
            channel.sendMessage("The users doesn't seem to be in a voice channel.").queue();
            return;
        }
        guild.moveVoiceMember(moveMeMember, targetMember.getVoiceState().getChannel()).queue();
    }
}
