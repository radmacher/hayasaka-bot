package moe.iam.hayasaka.command.misc;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;

import java.util.Date;

public class MoveMeObject {
    private final User requester;
    private final User addressee;
    private final Date timestamp;
    private final VoiceChannel channel;
    private final Message message;
    private final Guild guild;

    public MoveMeObject(User requester, User addressee, Date timestamp, VoiceChannel channel, Message message, Guild guild) {
        this.requester = requester;
        this.addressee = addressee;
        this.timestamp = timestamp;
        this.channel = channel;
        this.message = message;
        this.guild = guild;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public User getAddressee() {
        return addressee;
    }

    public User getRequester() {
        return requester;
    }

    public VoiceChannel getChannel() {
        return channel;
    }

    public Message getMessage() {
        return message;
    }

    public Guild getGuild() {
        return guild;
    }
}
