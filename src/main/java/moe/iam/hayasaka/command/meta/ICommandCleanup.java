package moe.iam.hayasaka.command.meta;

/**
 * Indicating that a command has data/cache to clean up after a while
 */
public interface ICommandCleanup {
    /**
     * This method is called in the cleanup service {@link BotCleanupService}
     * to clean up cached data
     */
    void cleanup();
}
