package moe.iam.hayasaka.command.meta;

public interface ICommandReactionListener<T> {

    CommandReactionListener<T> getReactionListener(long userId, T initialData);
}
