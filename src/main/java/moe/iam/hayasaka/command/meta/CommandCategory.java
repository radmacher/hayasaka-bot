package moe.iam.hayasaka.command.meta;

import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.util.Emojibet;

public enum CommandCategory {
    //CREATOR("creator", Emojibet.MAN_IN_SUIT, "Development", SimpleRank.CREATOR),
    BOT_ADMINISTRATION("bot_administration", Emojibet.MONKEY, "Bot administration", SimpleRank.BOT_ADMIN),
    SONGODA("songoda", Emojibet.STAR, "Songoda", SimpleRank.CREATOR),
    ADMINISTRATIVE("administrative", Emojibet.POLICE, "Administration", SimpleRank.GUILD_ADMIN),
    MEDIA("media", Emojibet.FILM, "Media", SimpleRank.MEDIA),
    MODERATION("moderation", Emojibet.DEFENSE, "Moderation", SimpleRank.MODERATOR),
    INFORMATIVE("informative", Emojibet.INFORMATION, "Information"),
    MUSIC("music", Emojibet.MUSIC_NOTE, "Music"),
    MISC("misc", Emojibet.NOTEPAD, "Miscellaneous"),
    USERS("users", Emojibet.USER, "Users"),
    //FUN("fun", Emojibet.GAME_DICE, "Fun"),
    //ANIME("anime", Emojibet.STAR, "Anime"),
    //DEVELOPMENT("development", Emojibet.WRENCH, "Development"),
    //NSFW("nsfw", Emojibet.X, "NSFW", SimpleRank.NSFW),
    UNKNOWN("nopackage", Emojibet.QUESTION_MARK, "Unknown");
    private final String packageName;
    private final String emoticon;
    private final String displayName;
    private final SimpleRank rankRequired;

    CommandCategory(String packageName, String emoticon, String displayName) {

        this.packageName = packageName;
        this.emoticon = emoticon;
        this.displayName = displayName;
        this.rankRequired = SimpleRank.USER;
    }

    CommandCategory(String packageName, String emoticon, String displayName, SimpleRank rankRequired) {

        this.packageName = packageName;
        this.emoticon = emoticon;
        this.displayName = displayName;
        this.rankRequired = rankRequired;
    }

    public static CommandCategory getFirstWithPermission(SimpleRank rank) {
        if (rank == null) {
            return INFORMATIVE;
        }
        for (CommandCategory category : values()) {
            if (rank.isAtLeast(category.getRankRequired())) {
                return category;
            }
        }
        return INFORMATIVE;
    }

    public static CommandCategory fromPackage(String packageName) {
        if (packageName != null) {
            for (CommandCategory cc : values()) {
                if (packageName.equalsIgnoreCase(cc.packageName)) {
                    return cc;
                }
            }
        }
        return UNKNOWN;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getEmoticon() {
        return emoticon;
    }

    public SimpleRank getRankRequired() {
        return rankRequired;
    }
}