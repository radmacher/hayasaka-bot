package moe.iam.hayasaka.command.meta;

public enum CooldownScope {
    USER(1), CHANNEL(2), GUILD(3), GLOBAL(4);

    private final int identifier;

    CooldownScope(int identifier) {

        this.identifier = identifier;
    }

    public int getId() {
        return identifier;
    }
}
