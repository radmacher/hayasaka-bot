package moe.iam.hayasaka.command.administrative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.model.OUser;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.templates.Templates;
import net.dv8tion.jda.api.entities.*;

import java.util.List;

public class RoleCommand extends AbstractCommand {
    /**
     * A short description of the method
     *
     * @return description
     */
    @Override
    public String getDescription() {
        return "Interact with roles";
    }

    /**
     * What should be typed to trigger this command (Without prefix)
     *
     * @return command
     */
    @Override
    public String getCommand() {
        return "role";
    }

    /**
     * How to use the command?
     *
     * @return command usage
     */
    @Override
    public String[] getUsage() {
        return new String[]{
                "role save"
        };
    }

    /**
     * aliases to call the command
     *
     * @return array of aliases
     */
    @Override
    public String[] getAliases() {
        return new String[0];
    }

    /**
     * @param bot          the shard where its executing on
     * @param args         arguments for the command
     * @param channel      channel where the command is executed
     * @param author       who invoked the command
     * @param inputMessage the incoming message object
     * @return the message to output or an empty string for nothing
     */
    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        if (args.length == 1) {
            if ("save".equals(args[0])) {
                channel.sendTyping().queue();
                saveRoles(inputMessage.getGuild());
                return ":floppy_disk: Successfully saved all roles to the database.";
            }
        }
        return Templates.invalid_use.formatGuild(channel);
    }

    private void saveRoles(Guild guild) {
        List<Member> members = guild.getMembers();
        for (Member member : members) {
            OUser user = CUser.findBy(member.getIdLong());
            //if user doesnt exist in db create a new one
            if (!user.discord_id.equals(member.getId())) {
                user.discord_id = member.getId();
                user.name = member.getUser().getName();
            }
            user.nickname = member.getEffectiveName(); //save nickname
            if (!member.getRoles().isEmpty()) {
                Role role = member.getRoles().get(0);
                if (role != null) {
                    user.setRoleId(role.getId());
                }
                CUser.update(user);
            }
        }
    }
}
