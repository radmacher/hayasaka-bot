package moe.iam.hayasaka.command.administrative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.Launcher;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

/**
 * !system
 * shows status of the bot's system
 */
public class SystemCommand extends AbstractCommand {
    public SystemCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "Shows memory usage";
    }

    @Override
    public String getCommand() {
        return "system";
    }

    @Override
    public String[] getUsage() {
        return new String[]{};
    }

    @Override
    public String[] getAliases() {
        return new String[]{
                "sysinfo",
                "sys"
        };
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {
        final Runtime runtime = Runtime.getRuntime();
        StringBuilder sb = new StringBuilder();
        long memoryLimit = runtime.maxMemory();
        long memoryInUse = runtime.totalMemory() - runtime.freeMemory();

        sb.append("System information: ").append("\n");
        sb.append("Running version: ").append("\n").append(Launcher.getVersion()).append("\n");
        sb.append("Memory").append("\n");
        sb.append(getProgressbar(memoryInUse, memoryLimit));
        sb.append(" [ ").append(numberInMb(memoryInUse)).append(" / ").append(numberInMb(memoryLimit)).append(" ]").append("\n");
        return sb.toString();
    }

    private String getProgressbar(long current, long max) {
        StringBuilder bar = new StringBuilder();
        final String BLOCK_INACTIVE = "▬";
        final String BLOCK_ACTIVE = ":blue_circle:";
        final int BLOCK_PARTS = 12;
        int activeBLock = (int) (((float) current / (float) max) * (float) BLOCK_PARTS);
        for (int i = 0; i < BLOCK_PARTS; i++) {
            if (i == activeBLock) {
                bar.append(BLOCK_ACTIVE);
            } else {
                bar.append(BLOCK_INACTIVE);
            }
        }
        return bar.toString();
    }

    private String numberInMb(long number) {
        return "" + (number / (1048576L)) + " mb";
    }

}