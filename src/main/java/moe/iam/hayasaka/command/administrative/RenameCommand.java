package moe.iam.hayasaka.command.administrative;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.font.FontUtil;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

public class RenameCommand extends AbstractCommand {

    public RenameCommand() {
        super();
    }

    @Override
    public String getDescription() {
        return "Rename a user";
    }

    @Override
    public String getCommand() {
        return "rename";
    }

    @Override
    public String[] getUsage() {
        return new String[]{
                "rename <@user> <name>"
        };
    }

    @Override
    public String[] getAliases() {
        return new String[]{
                "rname",
                "nick",
                "nickname"
        };
    }

    @Override
    public String execute(DiscordBot bot, String[] args, MessageChannel channel, User author, Message inputMessage) {

        return FontUtil.makeCursive("A a - B b - C c - D d - E e - F f - G g - H h - I i - J j - K k - L l - M m - N n - O o - P p - Q q - R r - S s - T t - U u - V v - W w - X x - Y y - Z z");
    }
}

