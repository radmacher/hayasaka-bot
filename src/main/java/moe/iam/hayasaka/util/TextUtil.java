package moe.iam.hayasaka.util;

import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.concurrent.TimeUnit;

public class TextUtil {
    public static String formatTiming(long timing, long maximum) {
        timing = Math.min(timing, maximum) / 1000;

        long seconds = timing % 60;
        timing /= 60;
        long minutes = timing % 60;
        timing /= 60;
        long hours = timing;

        if (maximum >= 3600000L) {
            return String.format("%d:%02d:%02d", hours, minutes, seconds);
        } else {
            return String.format("%d:%02d", minutes, seconds);
        }
    }

    public static String filter(String input){
        return input.replace("\u202E","")
                .replace("@everyone", "@\u0435veryone") // some unconventional e so it wont ping people
                .replace("@here", "@h\u0435re") // some unconventional e so it wont ping people
                .trim();
    }

    /**
     * send a self destroying message
     * @param channel the channel where the message should be send
     * @param message the message you want to send
     * @param deleteAfter after what delay the message should be deleted
     * @param timeUnit the time unit of the delay
     */
    public static void sendSelfDestroyingMessage(DiscordBot bot, TextChannel channel, String message, int deleteAfter, TimeUnit timeUnit) {
        if (channel.getIdLong() == bot.getMusicChannel(channel.getGuild().getIdLong()).getIdLong())
            channel.sendMessage(message).submit()
                    .thenCompose((m) -> m.delete().submitAfter(deleteAfter, timeUnit))
                    .whenComplete((s, error) -> {
                        if (error != null) error.printStackTrace();
                    });
        else
            channel.sendMessage(message).queue();
    }

    public static void sendSelfDestroyingMessage(DiscordBot bot, MessageChannel channel, String message, int deleteAfter, TimeUnit timeUnit) {
        TextChannel textChannel = (TextChannel) channel;
        if (textChannel != null)
            sendSelfDestroyingMessage(bot, textChannel, message, deleteAfter, timeUnit);
    }


}
