package moe.iam.hayasaka.util;

import moe.iam.hayasaka.main.Launcher;
import moe.iam.hayasaka.main.ProgramVersion;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateUtil {
    private static final Pattern versionPattern = Pattern.compile("version : '([0-9]+\\.[0-9]+\\.[0-9]+)'");

    public static ProgramVersion getLatestVersion() {
        String request = HttpHelper.doRequest("https://gitlab.com/Chicken-DEV/hayasaka-bot/-/raw/master/.gitlab-ci.yml");
        Matcher matcher = versionPattern.matcher(request);
        if (matcher.find()) {
            return ProgramVersion.fromString(matcher.group(1));
        }
        return Launcher.getVersion();
    }
}