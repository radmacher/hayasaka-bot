package moe.iam.hayasaka.util;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.settings.Settings;

public class HttpHelper {

    /**
     * @param url the url to request to
     * @return a string containing the response
     */
    public static String doRequest(String url) {
        try {
            return Unirest.get(url).header("User-Agent", Settings.USER_AGENT).asString().getBody();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return "";
    }
}