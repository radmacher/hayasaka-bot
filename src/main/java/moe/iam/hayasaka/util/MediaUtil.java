package moe.iam.hayasaka.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.settings.Settings;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MediaUtil {

    /**
     * merges an array of strings and makes it ready to be used in urls
     * @param urlList a list of strings you want to use in a url
     * @return one big string of all merged strings
     */
    public static String getUrlString(String... urlList) {
        StringBuilder query = new StringBuilder();
        if (urlList.length > 1) {
            for (String arg : urlList) {
                query.append(arg).append("%20");
            }
            query.delete(query.length() - 3, query.length());
        } else {
            query.append(urlList[0]);
        }
        return query.toString();
    }

    public static boolean isAnime(String... anime) {
        return getAnime(anime) != null;
    }

    public static JSONObject getAnime(String... animeToLookup) {
        return getAnime(getUrlString(animeToLookup));
    }

    /**
     * searches for an anime and returns the first result as a json object
     * @param animeToLookup the anime you want to search for
     * @return returns the first result as a json object or null if non could be found
     */
    public static JSONObject getAnime(String animeToLookup) {
        String malId = "";
        try {
            HttpResponse<JsonNode> response = Unirest.get("https://api.jikan.moe/v3/search/anime?q=" + getUrlString(animeToLookup)+"&page=1")
                    .asJson();
            JSONObject baseObj = (JSONObject) response.getBody().getObject();
            JSONArray base = (JSONArray) baseObj.get("results");
            if (base.length() == 0) return null;
            JSONObject anime = base.getJSONObject(0);
            if (!anime.has("mal_id")) return null;
            malId = anime.get("mal_id")+"";
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return getAnimeFromId(malId);
    }

    /**
     * retrieve an anime from its mal id
     * @param id the mal id
     * @return returns the response json object
     */
    public static JSONObject getAnimeFromId(String id) {
        try {
            HttpResponse<JsonNode> response = Unirest.get("https://api.jikan.moe/v3/anime/" + id)
                    .asJson();
            JSONObject anime = (JSONObject) response.getBody().getObject();
            if (anime.has("title")) {
                return anime;
            }
            else return null;
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * searches through all available {@link ContentType}
     * @param searchTerm the term you want to search for
     * @return returns a JSONArray of all results
     */
    public static JSONArray searchJellyfin(String searchTerm) {
        JSONArray result = new JSONArray();
        for (ContentType type : ContentType.values()) {
            JSONArray tmpResult = searchJellyfin(type, searchTerm);;
            if (tmpResult == null) continue;
            if (tmpResult.length() == 0) continue;
            try {
                for (int i = 0; i < tmpResult.length(); i++) {
                    JSONObject jsonObject = tmpResult.getJSONObject(i);
                    result.put(jsonObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * search for media on a jellyfin server
     * @param contentType the content type you are looking for
     * @param searchTerm the search term you want to search
     * @return returns a JSONArray of the result
     */
    public static JSONArray searchJellyfin(ContentType contentType, String searchTerm) {
        try {
            HttpResponse<JsonNode> response = Unirest.get(Settings.JELLYFIN_URL + "/Users/" + Settings.JELLYFIN_USER_ID +
                    "/Items?searchTerm=" + searchTerm + "&api_key=" + Settings.JELLYFIN_API_KEY + "&IncludeItemTypes=" + contentType.getApiName() + "&IncludeMedia=true&Recursive=true")
                    .asJson();
            JSONObject media = response.getBody().getObject();
            if (!media.has("Items"))
                return null;
            return media.getJSONArray("Items");
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * creates a jellyfin url from an itemid
     * @param itemId the items id you want to generate a url for
     * @return the url as a string
     */
    public static String jellyfinUrl(String itemId) {
        return Settings.JELLYFIN_URL + "/web/index.html#!/details?id=" + itemId;
    }

    /**
     * parse all valid results in a search result
     * @param searchResult the search result you want to parse
     * @return parsed {@link MediaObject} in a list
     */
    public static List<MediaObject> parseJellyfinSearch(JSONArray searchResult) {
        List<MediaObject> result = new ArrayList<>();
        for (int i = 0; i < searchResult.length(); i++) {
            result.add(parseJellyfinSearch(searchResult, i));
        }
        return result;
    }

    /**
     * parse a jellyfin search result
     * @param searchResult the JsonArray search result
     * @param index which result you want to parse
     * @return returns either null if no valid result could be found at the specified location or a {@link MediaObject}
     */
    public static MediaObject parseJellyfinSearch(JSONArray searchResult, int index) {
        if (searchResult.length() <= index) return null;
        JSONObject searchObject = searchResult.getJSONObject(index);
        if (!searchObject.has("Name")) return null;
        MediaObject record = new MediaObject();
        record.title = searchObject.getString("Name");
        record.itemId = searchObject.getString("Id");
        record.contentType = ContentType.getFromString(searchObject.getString("Type"));
        return record;
    }

    public static String getPrimaryImage(String itemId) {
        return Settings.JELLYFIN_URL + "/Items/" + itemId + "/Images/Primary";
    }

    /**
     * get the seasons of a show
     * @param itemId the show id
     * @return returns an array of the seasons or null if no seasons could be found
     */
    public static JSONArray getSeasons(String itemId) {
    try {
        HttpResponse<JsonNode> response = Unirest.get(Settings.JELLYFIN_URL + "/Shows/" + itemId + "/Seasons?userId=" + Settings.JELLYFIN_USER_ID + "&api_key=" + Settings.JELLYFIN_API_KEY)
                .asJson();
        JSONObject media = response.getBody().getObject();
        if (!media.has("Items"))
            return null;
        return media.getJSONArray("Items");
    } catch (UnirestException e) {
        e.printStackTrace();
    }
        return null;
    }

    /**
     * the different content types that can be searched for using the search method.
     * Currently only movies and series are implemented.
     */
    public enum ContentType {
        MOVIE("Movie"),
        SERIES("Series");

        private String apiName;
        ContentType(String apiName) {
            this.apiName = apiName;
        }

        public String getApiName() {
            return apiName;
        }

        /**
         * parse a content type from a String
         * @param string the string you want to parse
         * @return either returns a content type or null of none could be found
         */
        public static ContentType getFromString(String string) {
            for (ContentType type : ContentType.values()) {
                if (type.getApiName().equalsIgnoreCase(string)) return type;
            }
            return null;
        }
    }
}
