package moe.iam.hayasaka.util;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.MessageChannel;

public class DebugUtil {
    /**
     * Handles the debug output + response
     *
     * @param bot     the bot
     * @param channel the channel to send the messages to
     * @param output  the output to upload
     */
    public static void handleDebug(DiscordBot bot, MessageChannel channel, String output) {
        bot.queue.add(channel.sendMessage("One moment, uploading results: "),
                message -> {
                    String result = DebugUtil.sendToHastebin(output);
                    if (result == null) {
                        bot.queue.add(message.editMessage("Uploading failed!"));
                    } else {
                        bot.queue.add(message.editMessage("Here you go: " + result));
                    }
                });
    }

    /**
     * attempts to send the message to hastebin
     *
     * @param message the message to send
     * @return the url or null
     */
    public static String sendToHastebin(String message) {
        try {
            return "http://hastebin.com/" + handleHastebin(message);
        } catch (UnirestException ignored) {
        }
        return null;
    }

    /**
     * dumps a string to hastebin
     *
     * @param message the text to send
     * @return key how to find it
     */
    private static String handleHastebin(String message) throws UnirestException {
        return Unirest.post("https://hastebin.com/documents").body(message).asJson().getBody().getObject().getString("key");
    }
}
