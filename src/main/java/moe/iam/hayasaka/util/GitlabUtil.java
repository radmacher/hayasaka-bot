package moe.iam.hayasaka.util;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import moe.iam.hayasaka.settings.Settings;
import org.json.JSONArray;
import org.json.JSONObject;

public class GitlabUtil {
    //private static final String GITLAB_ID = "" + 20228371;
    //private static final String GITLAB_URL = "https://gitlab.com/api/v4";

    private static final String GITLAB_ID = Settings.GITLAB_PROJECT_ID;
    private static final String GITLAB_URL = Settings.GITLAB_URL;

    /**
     * get all pipelines of a project
     * @return returns an array of all pipelines
     */
    public static JSONArray getPipelines() {
        try {
            HttpResponse<JsonNode> response = Unirest.get(GITLAB_URL + "/projects/" + GITLAB_ID + "/pipelines?private_token=" + Settings.GITLAB_API_KEY)
                    .asJson();
            return response.getBody().getArray();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static JSONObject getLatestPipeline() {
        return getLatestPipeline(false);
    }

    public static JSONObject getLatestPipeline(boolean successful) {
        JSONArray pipelines = getPipelines();
        if (pipelines == null) return null;
        if (pipelines.length() < 1) return null;
        JSONObject latestPipeline = pipelines.getJSONObject(0);
        if (!latestPipeline.has("id")) return null;
        String successfulFilter = successful ? "&status=success" : "";
        try {
            HttpResponse<JsonNode> response = Unirest.get(GITLAB_URL + "/projects/" + GITLAB_ID + "/pipelines/" + latestPipeline.get("id") + "?private_token=" + Settings.GITLAB_API_KEY + successfulFilter)
                    .asJson();
            return response.getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static PipelineStatus latestPipelineStatus() {
        JSONObject latestPipeline = getLatestPipeline();
        if (latestPipeline == null) return null;
        if (!latestPipeline.has("status")) return null;
        return PipelineStatus.parseStatus(latestPipeline.get("status") + "");
    }

    public static JSONObject getLatestCommit() {
        return getCommitFromSHA(getLatestCommitSHA());
    }

    public static JSONObject getCommitFromSHA(String sha) {
        try {
            HttpResponse<JsonNode> response = Unirest.get(GITLAB_URL + "/projects/" + GITLAB_ID + "/repository/commits/" + sha + "?private_token=" + Settings.GITLAB_API_KEY)
                    .asJson();
            return response.getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getLatestCommitSHA() {
        JSONObject latestCommit = getCommits();
        if (latestCommit == null) return null;
        if (!latestCommit.has("id")) return null;
        return latestCommit.getString("id");
    }

    public static JSONObject getCommits() {
        try {
            HttpResponse<JsonNode> response = Unirest.get(GITLAB_URL + "/projects/" + GITLAB_ID + "/repository/commits?private_token=" + Settings.GITLAB_API_KEY)
                    .asJson();
            return response.getBody().getObject();
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    public enum PipelineStatus {
        PENDING,
        CANCELED,
        SUCCESS,
        FAILED;

        public static PipelineStatus parseStatus(String string) {
            for (PipelineStatus status : values()) {
                if (status.name().equalsIgnoreCase(string)) return status;
            }
            return null;
        }
    }
}
