package moe.iam.hayasaka.service;

import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.db.controllers.CTracker;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.model.OTracker;
import moe.iam.hayasaka.influxdb.InfluxManager;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.settings.Settings;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class TrackerService extends AbstractService {

    public TrackerService(BotContainer b) {
        super(b);
    }

    /**
     * The identifier of the service. This is used to reference the service and the key to store data with.
     *
     * @return the identifier of the service
     */
    @Override
    public String getIdentifier() {
        return "tracker_service";
    }

    /**
     * milliseconds it should wait before attempting another run
     *
     * @return delay in milliseconds
     */
    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.SECONDS.toMillis(60);
    }

    /**
     * Determines if the service should run
     *
     * @return should it run?
     */
    @Override
    public boolean shouldIRun() {
        return Settings.ENABLE_TRACKER;
    }

    /**
     * called before run, so things can be prepared if needed
     */
    @Override
    public void beforeRun() {

    }

    /**
     * the actual logic of the service
     */
    @Override
    public void run() throws Exception {
        DiscordBot[] shards = bot.getShards();
        List<Member> membersOnline = new ArrayList<>();
        for (DiscordBot shard : shards) {
            if (shard == null || !shard.isReady()) {
                continue;
            }
            for (Guild guild : shard.getJda().getGuilds()) {
                membersOnline.addAll(guild.getMembers());

                //user status tracker
                int online, idle, dnd, offline, unknown;
                online = idle = dnd = offline = unknown = 0;
                HashMap<Member, OnlineStatus> statusList = new HashMap<>();
                guild.getMembers().forEach(member -> statusList.put(member, member.getOnlineStatus()));
                for (Member member : statusList.keySet()) {
                    if (member.getUser().isBot())
                        continue;
                    OnlineStatus status = member.getOnlineStatus();
                    switch (status) {
                        case ONLINE:
                            membersOnline.add(member);
                            online++;
                            break;
                        case IDLE:
                            membersOnline.add(member);
                            idle++;
                            break;
                        case DO_NOT_DISTURB:
                            membersOnline.add(member);
                            dnd++;
                            break;
                        case OFFLINE:
                        case INVISIBLE:
                            offline++;
                            break;
                        case UNKNOWN:
                        default:
                            unknown++;
                            break;
                    }
                }
                OTracker record = new OTracker();
                record.guildId = guild.getIdLong();
                record.date = new Date(System.currentTimeMillis());
                record.usersOnline = online;
                record.usersIdle = idle;
                record.usersDnD = dnd;
                record.usersOffline = offline;
                record.usersUnknown = unknown;
                CTracker.insert(record);
                if (InfluxManager.enabled) {
                    System.out.println("Adding data to influx.");
                    InfluxManager.addData(record);
                }
            }
        }
        for (Member member : membersOnline) {
            CUser.addMinuteOnline(member.getUser().getIdLong());
        }
    }

    /**
     * called after run(), can be used to clean up things if needed
     */
    @Override
    public void afterRun() {
    }
}
