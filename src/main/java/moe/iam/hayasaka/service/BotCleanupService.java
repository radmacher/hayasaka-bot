package moe.iam.hayasaka.service;

import moe.iam.hayasaka.command.meta.AbstractCommand;
import moe.iam.hayasaka.command.meta.ICommandCleanup;
import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.handler.CommandHandler;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;

import java.util.concurrent.TimeUnit;

/**
 * delete cached stuff, etc.
 */
public class BotCleanupService extends AbstractService {
    private int runCount = 0;

    public BotCleanupService(BotContainer b) {
        super(b);
    }

    @Override
    public String getIdentifier() {
        return "bot_cleanup_service";
    }

    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.MINUTES.toMillis(1);
    }

    @Override
    public boolean shouldIRun() {
        return bot.allShardsReady();
    }

    @Override
    public void beforeRun() {
    }

    @Override
    public void run() {
        runCount++;
        for (DiscordBot shard : bot.getShards()) {
            if (shard == null || !shard.isReady()) {
                continue;
            }
            shard.commandReactionHandler.cleanCache();
            //TODO: implement cleanup for chat bot
            //shard.chatBotHandler.cleanCache();
            shard.gameHandler.cleanCache();
        }
        if (runCount < 60) {
            return;
        }
        runCount = 0;
        for (AbstractCommand abstractCommand : CommandHandler.getCommandObjects()) {
            if (abstractCommand instanceof ICommandCleanup) {
                ((ICommandCleanup) abstractCommand).cleanup();
            }
        }
    }

    @Override
    public void afterRun() {
    }
}