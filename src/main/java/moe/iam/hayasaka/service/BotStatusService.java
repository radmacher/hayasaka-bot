package moe.iam.hayasaka.service;

import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.settings.Settings;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.TextChannel;

import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * pseudo randomly sets the now playing tag of the bot
 */
public class BotStatusService extends AbstractService {
    private final static String[] statusList = {
            "Hey, Hey! %s",
    };
    private final Random rng;

    public BotStatusService(BotContainer b) {
        super(b);
        rng = new Random();
    }

    @Override
    public String getIdentifier() {
        return "bot_nickname";
    }

    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.SECONDS.toMillis(90);
    }

    @Override
    public boolean shouldIRun() {
        return !bot.isStatusLocked();
    }

    @Override
    public void beforeRun() {
    }

    @Override
    public void run() {
        int roll = rng.nextInt(100);
        TextChannel inviteChannel = bot.getShardFor(Settings.BOT_GUILD_ID).getJda().getTextChannelById(Settings.BOT_CHANNEL_ID);
        if (inviteChannel != null && roll < 10) {
            String fallback = "@ https://discord.gg/ywFV2EB | #%s";
            bot.getShardFor(Settings.BOT_GUILD_ID).queue.add(inviteChannel.retrieveInvites(),
                    invites -> {
                        if (invites != null && !invites.isEmpty()) {
                            setGameOnShards(bot, "@ https://discord.gg/" + invites.get(0).getCode() + " | %s");
                        } else {
                            setGameOnShards(bot, fallback);
                        }
                    });
        } else if (roll < 50) {
            String username = bot.getShards()[0].getJda().getSelfUser().getName();
            setGameOnShards(bot, "@" + username + " help | #%s");
        } else {
            setGameOnShards(bot, statusList[new Random().nextInt(statusList.length)]);
        }
    }

    private void setGameOnShards(BotContainer container, String status) {
        for (DiscordBot shard : container.getShards()) {
            switch (Settings.BOT_ENV) {
                case "RELEASE":
                    shard.getJda().getPresence().setPresence(OnlineStatus.ONLINE, Activity.playing(String.format(status, shard.getShardId())));
                    break;
                case "BETA":
                    shard.getJda().getPresence().setPresence(OnlineStatus.IDLE, Activity.playing(String.format(status, shard.getShardId())));
                    break;
                case "ALPHA":
                    shard.getJda().getPresence().setPresence(OnlineStatus.DO_NOT_DISTURB, Activity.playing(String.format(status, shard.getShardId())));
                    break;
                default:
                    shard.getJda().getPresence().setPresence(OnlineStatus.OFFLINE, Activity.playing(String.format(status, shard.getShardId())));
                    break;
            }
            //shard.getJda().getPresence().setActivity(Activity.playing(String.format(status, shard.getShardId())));
        }
    }

    @Override
    public void afterRun() {
    }
}