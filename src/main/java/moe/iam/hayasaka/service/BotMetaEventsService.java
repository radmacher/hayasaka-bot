package moe.iam.hayasaka.service;

import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.db.controllers.CBotEvent;
import moe.iam.hayasaka.db.controllers.CBotStat;
import moe.iam.hayasaka.db.model.OBotEvent;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.Launcher;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Bot meta events
 */
public class BotMetaEventsService extends AbstractService {
    private SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    public BotMetaEventsService(BotContainer b) {
        super(b);
    }

    @Override
    public String getIdentifier() {
        return "bot_meta_events";
    }

    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.MINUTES.toMillis(5);
    }

    @Override
    public boolean shouldIRun() {
        return true;
    }

    @Override
    public void beforeRun() {
    }

    @Override
    public void run() {
        int lastId = Integer.parseInt("0" + getData("last_broadcast_id"));
        List<OBotEvent> events = CBotEvent.getEventsAfter(lastId);
        List<TextChannel> subscribedChannels = getSubscribedChannels();
        int totGuilds = 0, totUsers = 0, totChannels = 0, totVoice = 0, totActiveVoice = 0;
        for (DiscordBot shard : bot.getShards()) {
            List<Guild> guilds = shard.getJda().getGuilds();
            int numGuilds = guilds.size();
            int users = shard.getJda().getUsers().size();
            int channels = shard.getJda().getTextChannels().size();
            int voiceChannels = shard.getJda().getVoiceChannels().size();
            int activeVoice = 0;
            for (Guild guild : shard.getJda().getGuilds()) {
                if (guild.getAudioManager().isConnected()) {
                    activeVoice++;
                }
            }
            totGuilds += numGuilds;
            totUsers += users;
            totChannels += channels;
            totVoice += voiceChannels;
            totActiveVoice += activeVoice;
        }
        CBotStat.insert(totGuilds, totUsers, totActiveVoice);
        Launcher.log("Statistics", "bot", "meta-stats",
                "guilds", totGuilds,
                "users", totUsers,
                "channels", totChannels,
                "voice-channels", totVoice,
                "radio-channels", totActiveVoice
        );

        if (events.isEmpty()) {
            return;
        }
        for (OBotEvent event : events) {
            String output = String.format(":watch: `%s` %s %s %s", dateFormat.format(event.createdOn), event.group, event.subGroup, event.data);
            for (TextChannel channel : subscribedChannels) {
                sendTo(channel, output);
            }
            lastId = event.id;
        }
        saveData("last_broadcast_id", lastId);

    }

    @Override
    public void afterRun() {
    }
}