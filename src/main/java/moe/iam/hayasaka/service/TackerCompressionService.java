package moe.iam.hayasaka.service;

import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.db.controllers.CTracker;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;
import net.dv8tion.jda.api.entities.Guild;

import java.util.concurrent.TimeUnit;

public class TackerCompressionService extends AbstractService {

    public TackerCompressionService(BotContainer b) {
        super(b);
    }

    /**
     * The identifier of the service. This is used to reference the service and the key to store data with.
     *
     * @return the identifier of the service
     */
    @Override
    public String getIdentifier() {
        return "tracker_compression_service";
    }

    /**
     * milliseconds it should wait before attempting another run
     *
     * @return delay in milliseconds
     */
    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.MINUTES.toMillis(45);
    }

    /**
     * Determines if the service should run
     *
     * @return should it run?
     */
    @Override
    public boolean shouldIRun() {
        return true;
    }

    /**
     * called before run, so things can be prepared if needed
     */
    @Override
    public void beforeRun() {}

    /**
     * the actual logic of the service
     */
    @Override
    public void run() throws Exception {
        for (DiscordBot shard : bot.getShards()) {
            if (shard == null || !shard.isReady()) {
                continue;
            }
            for (Guild guild : shard.getJda().getGuilds()) {
                CTracker.compressOldData(guild.getIdLong());
            }
        }
    }

    /**
     * called after run(), can be used to clean up things if needed
     */
    @Override
    public void afterRun() {}
}
