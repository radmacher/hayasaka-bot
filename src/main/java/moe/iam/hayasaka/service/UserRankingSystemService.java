package moe.iam.hayasaka.service;

import moe.iam.hayasaka.core.AbstractService;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.handler.GuildSettings;
import moe.iam.hayasaka.main.BotContainer;
import moe.iam.hayasaka.main.DiscordBot;
import moe.iam.hayasaka.main.Launcher;
import moe.iam.hayasaka.role.RoleRankings;
import net.dv8tion.jda.api.entities.Guild;

import java.util.List;
import java.util.concurrent.TimeUnit;
/**
 * updates the ranking of members within a guild
 */
public class UserRankingSystemService extends AbstractService {

    public UserRankingSystemService(BotContainer b) {
        super(b);
    }

    @Override
    public String getIdentifier() {
        return "user_role_ranking";
    }

    @Override
    public long getDelayBetweenRuns() {
        return TimeUnit.MINUTES.toMillis(15);
    }

    @Override
    public boolean shouldIRun() {
        return true;
    }

    @Override
    public void beforeRun() {
    }

    @Override
    public void run() {
        for (DiscordBot discordBot : bot.getShards()) {
            List<Guild> guilds = discordBot.getJda().getGuilds();
            for (Guild guild : guilds) {
                GuildSettings settings = GuildSettings.get(guild);
                if (settings != null && settings.getBoolValue(GSetting.USER_TIME_RANKS) && RoleRankings.canModifyRoles(guild, discordBot.getJda().getSelfUser())) {
                    try {
                        handleGuild(discordBot, guild);
                    } catch (Exception e) {
                        Launcher.logToDiscord(e, "guild", guild.getId(), "name", guild.getName());
                    }
                }
            }
        }
    }

    private void handleGuild(DiscordBot bot, Guild guild) {
        RoleRankings.fixForServer(guild);
        guild.getMembers().stream().filter(user -> !user.getUser().isBot()).forEach(user -> RoleRankings.assignUserRole(bot, guild, user.getUser()));
    }

    @Override
    public void afterRun() {
        System.gc();
    }
}