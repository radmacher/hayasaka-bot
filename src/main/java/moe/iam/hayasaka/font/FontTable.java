package moe.iam.hayasaka.font;

public enum FontTable {

    //𝓐 𝓪𝓑 𝓫𝓒 𝓬𝓓 𝓭𝓔 𝓮𝓕 𝓯𝓖 𝓰𝓗 𝓱𝓘 𝓲𝓙 𝓳𝓚 𝓴𝓛 𝓵𝓜 𝓶𝓝 𝓷𝓞 𝓸𝓟 𝓹𝓠 𝓺𝓡 𝓻𝓢 𝓼𝓣 𝓽𝓤 𝓾𝓥 𝓿𝓦 𝔀𝓧 𝔁𝓨 𝔂𝓩 𝔃
    A("\uD835\uDCD0", "\uD835\uDD6C"),
    a("\uD835\uDCEA", "\uD835\uDD86"),
    B("\uD835\uDCD1", "\uD835\uDD6D"),
    b("\uD835\uDCEB", "\uD835\uDD87"),
    C("\uD835\uDCD2", "\uD835\uDD6E"),
    c("\uD835\uDCEC", "\uD835\uDD88"),
    D("\uD835\uDCD3", "\uD835\uDD6F"),
    d("\uD835\uDCED", "\uD835\uDD89"),
    E("\uD835\uDCD4", "\uD835\uDD70"),
    e("\uD835\uDCEE", "\uD835\uDD8A"),
    F("\uD835\uDCD5", "\uD835\uDD71"),
    f("\uD835\uDCEF", "\uD835\uDD8B"),
    G("\uD835\uDCD6", "\uD835\uDD72"),
    g("\uD835\uDCF0", "\uD835\uDD8C"),
    H("\uD835\uDCD7", "\uD835\uDD73"),
    h("\uD835\uDCF1", "\uD835\uDD8D"),
    I("\uD835\uDCD8", "\uD835\uDD74"),
    i("\uD835\uDCF2", "\uD835\uDD8E"),
    J("\uD835\uDCD9", "\uD835\uDD75"),
    j("\uD835\uDCF3", "\uD835\uDD8F"),
    K("\uD835\uDCDA", "\uD835\uDD76"),
    k("\uD835\uDCF4", "\uD835\uDD90"),
    L("\uD835\uDCDB", "\uD835\uDD77"),
    l("\uD835\uDCF5", "\uD835\uDD91"),
    M("\uD835\uDCDC", "\uD835\uDD78"),
    m("\uD835\uDCF6", "\uD835\uDD92"),
    N("\uD835\uDCDD", "\uD835\uDD79"),
    n("\uD835\uDCF7", "\uD835\uDD93"),
    O("\uD835\uDCDE", "\uD835\uDD7A"),
    o("\uD835\uDCF8", "\uD835\uDD94"),
    P("\uD835\uDCDF", "\uD835\uDD7B"),
    p("\uD835\uDCF9", "\uD835\uDD95"),
    Q("\uD835\uDCE0", "\uD835\uDD7C"),
    q("\uD835\uDCFA", "\uD835\uDD96"),
    R("\uD835\uDCE1", "\uD835\uDD7D"),
    r("\uD835\uDCFB", "\uD835\uDD97"),
    S("\uD835\uDCE2", "\uD835\uDD7E"),
    s("\uD835\uDCFC", "\uD835\uDD98"),
    T("\uD835\uDCE3", "\uD835\uDD7F"),
    t("\uD835\uDCFD", "\uD835\uDD99"),
    U("\uD835\uDCE4", "\uD835\uDD80"),
    u("\uD835\uDCFE", "\uD835\uDD9A"),
    V("\uD835\uDCE5", "\uD835\uDD81"),
    v("\uD835\uDCFF", "\uD835\uDD9B"),
    W("\uD835\uDCE6", "\uD835\uDD82"),
    w("\uD835\uDD00", "\uD835\uDD9C"),
    X("\uD835\uDCE7", "\uD835\uDD83"),
    x("\uD835\uDD01", "\uD835\uDD9D"),
    Y("\uD835\uDCE8", "\uD835\uDD84"),
    y("\uD835\uDD02", "\uD835\uDD9E"),
    Z("\uD835\uDCE9", "\uD835\uDD85"),
    z("\uD835\uDD03", "\uD835\uDD9F");

    private String cursiveCharacter;
    private String germanicCharacter;

    FontTable(String cursiveCharacter, String germanicCharacter) {
        this.cursiveCharacter = cursiveCharacter;
        this.germanicCharacter = germanicCharacter;
    }

    public String getCursiveCharacter() {
        return cursiveCharacter;
    }

    public String getGermanicCharacter() {
        return germanicCharacter;
    }
}
