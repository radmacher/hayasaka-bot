package moe.iam.hayasaka.font;

import java.util.ArrayList;

public class FontUtil {

    //TODO: there probably is a better way to do this
    public static String makeCursive(String message) {
        StringBuilder msg = new StringBuilder();
        ArrayList<String> availableChars = new ArrayList<>();
        for (FontTable table : FontTable.values()) {
            if (table.getCursiveCharacter() != null) {
                availableChars.add(table.name());
            }
        }
        chars: for (char c : message.toCharArray()) {
            for (FontTable table : FontTable.values()) {
                if (!availableChars.contains(c+"")) {
                    msg.append(c);
                    continue chars;
                }
                if (table.name().toString().equals(c+"")) {
                    msg.append(table.getCursiveCharacter());
                    continue chars;
                }
            }
        }
        return msg.toString();
    }
}
