package moe.iam.hayasaka.handler.audio;

import com.sedmelluq.discord.lavaplayer.track.AudioTrack;

public interface TrackOperation {
    void execute(AudioTrack track);
}
