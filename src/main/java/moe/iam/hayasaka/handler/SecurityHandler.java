package moe.iam.hayasaka.handler;

import moe.iam.hayasaka.db.controllers.CGuild;
import moe.iam.hayasaka.db.controllers.CRank;
import moe.iam.hayasaka.db.controllers.CUser;
import moe.iam.hayasaka.db.controllers.CUserRank;
import moe.iam.hayasaka.db.model.OGuild;
import moe.iam.hayasaka.db.model.OUserRank;
import moe.iam.hayasaka.guildsettings.GSetting;
import moe.iam.hayasaka.main.GuildCheckResult;
import moe.iam.hayasaka.permission.SimpleRank;
import moe.iam.hayasaka.settings.Settings;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.*;
import net.dv8tion.jda.internal.utils.PermissionUtil;

import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Manages permissions/bans for discord
 */
public class SecurityHandler {
    private final static HashSet<Long> bannedGuilds = new HashSet<>();
    private final static HashSet<Long> bannedUsers = new HashSet<>();
    private final static HashSet<Long> interactionBots = new HashSet<>();
    private final static HashSet<Long> contributors = new HashSet<>();
    private final static HashSet<Long> botAdmins = new HashSet<>();
    private final static HashSet<Long> systemAdmins = new HashSet<>();

    public SecurityHandler() {
    }

    public static synchronized void initialize() {
        bannedGuilds.clear();
        bannedUsers.clear();
        interactionBots.clear();
        contributors.clear();
        botAdmins.clear();
        systemAdmins.clear();
        List<OGuild> bannedList = CGuild.getBannedGuilds();
        bannedGuilds.addAll(bannedList.stream().map(guild -> guild.discord_id).collect(Collectors.toList()));
        CUser.addBannedUserIds(bannedUsers);

        List<OUserRank> interaction_bots = CUserRank.getUsersWith(CRank.findBy("INTERACTION_BOT").id);
        List<OUserRank> contributor = CUserRank.getUsersWith(CRank.findBy("CONTRIBUTOR").id);
        List<OUserRank> bot_admin = CUserRank.getUsersWith(CRank.findBy("BOT_ADMIN").id);
        List<OUserRank> system_admin = CUserRank.getUsersWith(CRank.findBy("SYSTEM_ADMIN").id);
        contributors.addAll(contributor.stream().map(oUserRank -> CUser.getCachedDiscordId(oUserRank.userId)).collect(Collectors.toList()));
        interactionBots.addAll(interaction_bots.stream().map(oUserRank -> CUser.getCachedDiscordId(oUserRank.userId)).collect(Collectors.toList()));
        botAdmins.addAll(bot_admin.stream().map(oUserRank -> CUser.getCachedDiscordId(oUserRank.userId)).collect(Collectors.toList()));
        systemAdmins.addAll(system_admin.stream().map(oUserRank -> CUser.getCachedDiscordId(oUserRank.userId)).collect(Collectors.toList()));
    }

    public boolean isInteractionBot(long userId) {
        return interactionBots.contains(userId);
    }

    public boolean isBanned(User user) {
        return bannedUsers.contains(user.getIdLong());
    }

    public synchronized void addUserBan(long discordId) {
        if (!bannedUsers.contains(discordId)) {
            bannedUsers.add(discordId);
        }
    }

    public synchronized void removeUserBan(long discordId) {
        if (bannedUsers.contains(discordId)) {
            bannedUsers.remove(discordId);
        }
    }

    public boolean isBanned(Guild guild) {
        return bannedGuilds.contains(guild.getIdLong());
    }

    public SimpleRank getSimpleRank(User user) {
        return getSimpleRankForGuild(user, null);
    }

    public SimpleRank getSimpleRank(User user, MessageChannel channel) {
        if (channel instanceof TextChannel) {
            return getSimpleRankForGuild(user, ((TextChannel) channel).getGuild());
        }
        return getSimpleRankForGuild(user, null);
    }

    /**
     * Try and figure out what type of guild it is
     *
     * @param guild the guild to check
     * @return what category the guild is labeled as
     */
    public GuildCheckResult checkGuild(Guild guild) {

        int bots = 0;
        int users = 0;
        for (Member user : guild.getMembers()) {
            if (user.getUser().isBot()) {
                bots++;
            }
            users++;
        }
        if ((double) bots / users > Settings.GUILD_MAX_USER_BOT_RATIO) {
            return GuildCheckResult.BOT_GUILD;
        }
        if (users < Settings.GUILD_MIN_USERS) {
            return GuildCheckResult.TEST_GUILD;
        }
        return GuildCheckResult.OKE;
    }

    public SimpleRank getSimpleRankForGuild(User user, Guild guild) {
        long userId = user.getIdLong();
        if (user.getIdLong() == Settings.CREATOR_ID) {
            return SimpleRank.CREATOR;
        }
        if (user.isBot()) {
            if (interactionBots.contains(userId)) {
                return SimpleRank.INTERACTION_BOT;
            }
            return SimpleRank.BOT;
        }
        if (systemAdmins.contains(userId)) {
            return SimpleRank.SYSTEM_ADMIN;
        }
        if (botAdmins.contains(userId)) {
            return SimpleRank.BOT_ADMIN;
        }
        if (contributors.contains(userId)) {
            return SimpleRank.CONTRIBUTOR;
        }
        if (bannedUsers.contains(userId)) {
            return SimpleRank.BANNED_USER;
        }
        if (guild != null) {
            if (guild.getOwner().equals(user)) {
                return SimpleRank.GUILD_OWNER;
            }
            if (PermissionUtil.checkPermission(guild.getMember(user), Permission.ADMINISTRATOR)) {
                return SimpleRank.GUILD_ADMIN;
            }
            Role role = GuildSettings.get(guild).getRoleValue(GSetting.BOT_ADMIN_ROLE, guild);
            if (role != null && guild.getMember(user).getRoles().contains(role)) {
                return SimpleRank.GUILD_BOT_ADMIN;
            }
        }
        return SimpleRank.USER;
    }

    public boolean isBotAdmin(long discordUserId) {
        return botAdmins.contains(discordUserId);
    }
}
